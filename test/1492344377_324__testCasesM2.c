//1.

struct hello {
        int a;
        float b;
};

int main() {
        int b;
        int a;
        struct hello k;

        a = k.a;

        return 0;
}
/*
------------------------------------------------------------------------------------------

2.

struct hello {
        int a;
        float b;
        int d;
};

int main() {
        int b;
        int a;
        struct hello k;  

        a = k.d;

        return 0;
}

3.

struct hello {
        int a;
        float b;
};

int main() {
        int b;
        int a;
        struct hello k;  

        a = k.a;

        return 0;
}

4.

struct hello {
        int a;
        float b;
};

struct hello_world {
        int a;
        float b;
};

int get_a(struct hello k) {
        return k.a;
}

int main() {
        int b;
        int a;
        struct hello k;  

        a = get_a(k);

        return 0;
}


5.

Scoping and Basic Type Checks and Casting

struct s {
  int a;
  float b;
};

struct t {
  int a;
  float b;
};

void g(int x, float y) {
  printf(x);
  printf("\n");
  printf(y);
}

void main() {
  struct s x;
  struct s y;
  float q;
  x.b = 2.0;
  y = x;
  x.b = -4;
  g(x.b, y.b);
}


6. Arrays Type Checking

struct s {
    int a;
    float b[5][5];
};

struct t{
    struct s x[10];
};

void f(int * a[10], float b[8][5]) {
    b[1][2] = 10.4;
}

void main() {
    float a[10][10];
    int b[10][10];
    int * c[5];
    struct t y;
    int i;
    float * fp;
    y.x[3].b[1][2] = 4.8;
    f(c,y.x[3].b);
    print(y.x[3].b[1][2]);
}


7. Pointers Type Checking and Casting

struct s {
    int a;
};

int * f(void * a) {
    return a;
}

void main() {
    struct s a, *p;
    int * ip;
    a.a = 3;
    p = &a;
    ip = f(p);
    print(*ip);
}

EXTRA SYNTACTIC CHECKS
========================
1.

int main()
{
  int a;
  a= 5;
  a++;
  printf(a);
}

2. 

int main()
{
  int a[7];
  printf(a[0]);
  a[1] = 36542;
  printf(a[1]);
  return 0;

}

3.
int f (int b ) {     
    int a;
    a=5;
    return a;
}

int main () {
    int a,b;
    a = 9;
    b = f(a);
    printf(a,”\n”,b);
}

4. 
int f (int a) {     
    int c;
    c=5;
    return a ;
}

int main () {
    int a,b;
    a = 5;
    b = f(a);
}

5.
int main()
{
  int i;
  int j;
  j = i+ j++; 
 print(j);
}
--------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------
6.

int returnInt () {
    return 1;
}

int main() {
    int a;
    int b;
    int c[3];

    a = 2;
    b = 4;
        
    *(&a) = b; 

    c[returnInt()] = 4;
   printf(c[1]);

}

Overloading resolution
========================
1.
struct S
{
    int a;
    float b;
};
int main()
{
    float f1,f2,f3;
    float fa[3];
    int ia[6];
    int i1,i2;
    struct S x;
f1 = f2 = 3;
x.a = 5;
x.b = 7;
fa[0] = 2;
ia[5] = 9;
i2 = 1;
    i1 = f1 + f2;       // i1 = (int) f1 +FLOAT f2
    f3 = f1 / i2;       // f3 =  f1 /FLOAT ((float) i2)
    f1 = i1 * x.a;      // f1 = (float) i1 +INT x.a
    i1 = fa[0] + ia[5]; // i1 = (int) fa[0] +FLOAT ((float) ia[0])
printf(i1);
printf(f3);
printf(f1);
}


==========================
2.
﻿float returnFloat () {
    return 2.4;
}


int main () {
    int a;
    int b;
    int c[3];


    c[ returnFloat() ] = 1;


}




=======================

3.
int* returnPointerToInt () {
    int* a;
    return a;
}


int main () {
    int a;
    int b;
    int c[3];


    c[ returnPointerToInt() ] = 2;
}


===========================

4.
int main() {
    int a;
    1++;
}


===========================

5.
int f(int a) {
    return a+1;
}


void main() {
    f(4) = 4;
}


===========================

6.
int f(int a) {
    return a+1;
}
int main() {
    int f, g;
    g = f(4);
    return 0;
}
*/