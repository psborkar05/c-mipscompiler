#!/usr/bin/env python

import sys

import ply.lex as lex

reserved = (
"AUTO","BREAK", "CASE","CHAR","CONST","CONTINUE", "DEFAULT","DO","DOUBLE","ELSE","ENUM",
"EXTERN","FLOAT","FOR","GOTO","IF","INT","LONG","REGISTER","RETURN",
"SHORT","SIGNED","SIZEOF","STATIC","STRUCT","SWITCH","TYPEDEF","UNION","UNSIGNED","VOID","VOLATILE","WHILE"
)

tokens = reserved + (
	 "IDENTIFIER", "FLOATNO", "INTEGERNO","STRING_LITERAL", "PTR_OP", "INC_OP", "DEC_OP", "LEFT_OP", "RIGHT_OP",
	 "LE_OP", "GE_OP", "EQ_OP", "NE_OP","AND_OP", "OR_OP","ELLIPSIS", "MUL_ASSIGN", "DIV_ASSIGN",
	 "MOD_ASSIGN", "ADD_ASSIGN","SUB_ASSIGN", "LEFT_ASSIGN", "RIGHT_ASSIGN", "AND_ASSIGN",
	 "XOR_ASSIGN", "OR_ASSIGN", "TYPE_NAME","PLUS", "MINUS", "TIMES", "DIVIDE", "MOD", "XOR", "AND", "OR",
	 "NOT", "LNOT", "LT", "GT", "EQUALS", "COLON", "LBRACKET", "RBRACKET", "LBRACE", "RBRACE", "LPAREN",
	 "RPAREN", "QUEMARK", "PERIOD", "SEMI", "COMMA"
	) 

def t_NEWLINE(t):
    r'\n+'
    t.lexer.lineno += t.value.count("\n")
t_ignore = ' \t\x0c'

t_PTR_OP = r'->'
t_ELLIPSIS = r'\.\.\.'
t_LEFT_OP = r'<<'
t_RIGHT_OP = r'>>'
t_EQ_OP = r'=='
t_NE_OP = r'!='
t_LE_OP = r'<='
t_GE_OP = r'>='
t_AND_OP = r'&&'
t_OR_OP = r'\|\|'
t_INC_OP = r'\+\+'
t_DEC_OP = r'--'
t_ADD_ASSIGN = r'\+='
t_SUB_ASSIGN = r'-='
t_MUL_ASSIGN = r'\*='
t_DIV_ASSIGN = r'/='
t_MOD_ASSIGN = r'%='
t_XOR_ASSIGN = r'\^='
t_AND_ASSIGN = r'&='
t_OR_ASSIGN = r'\|='
t_RIGHT_ASSIGN = r'>>='
t_LEFT_ASSIGN = r'<<='

#literals = [ '+', '-', '*', '/', '%', '^', '&', '|', '~', '!', '<',  '>', '=', ':', '[', ']', '{', '}', '(', ')', '?', '.', ';', ',']

t_PLUS = r'\+'
t_MINUS = r'-'
t_TIMES = r'\*'
t_DIVIDE = r'/'
t_MOD = r'%'
t_XOR = r'\^'
t_AND = r'&'
t_OR = r'\|'
t_NOT = r'~'
t_LNOT = r'!'
t_LT = r'<'
t_GT = r'>'
t_EQUALS = r'='
t_COLON = r':'
t_LBRACKET = r'\['
t_RBRACKET = r'\]'
t_LBRACE = r'\{'
t_RBRACE = r'\}'
t_LPAREN = r'\('
t_RPAREN = r'\)'
t_QUEMARK = r'\?'
t_PERIOD = r'\.'
t_SEMI = r';'
t_COMMA = r','

reserved_map = {}

for r in reserved:
    reserved_map[r.lower()] = r


def t_IDENTIFIER(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = reserved_map.get(t.value, "IDENTIFIER")
    return t

# Integer literal
t_INTEGERNO = r'\d+([uU]|[lL]|[uU][lL]|[lL][uU])?'

# Floating literal
t_FLOATNO = r'((\d+)(\.\d+)(e(\+|-)?(\d+))? | (\d+)e(\+|-)?(\d+))([lL]|[fF])?'
stringtext = r'([^"\n\\])|(\\.)'
t_STRING_LITERAL = r'[a-zA-Z_]?"(' + stringtext + r')*"'

def t_comment(t):
    r'/\*(.|\n)*?\*/'
    t.lexer.lineno += t.value.count('\n')

# Preprocessor directive (ignored)
def t_preprocessor(t):
    r'\#(.)*?\n'
    t.lexer.lineno += 1

def t_error(t):
    print("Illegal character %s" % repr(t.value[0]))
    t.lexer.skip(1)

lexer = lex.lex()

if __name__ == "__main__":
    lex.runmain(lexer)
