class ThreeAddressCode:

	def __init__(self,ST):
		self.code = {
			"program":{"tac":[],
						"startno":0}
		}
		self.nextQuad = 1
		self.labelCount = -1
		self.labelPrefix = "L"
		self.tempCount = -1
		self.tempPrefix = "T"
		self.ST = ST

	def emit0(self,funcName):
		currScope = self.ST.scopeList[-1]["scope"]
		self.code[currScope]["tac"].append([funcName])
	
	def emitf(self,op1,op2):
		currScope = self.ST.scopeList[-1]["scope"]
		self.code[currScope]["tac"].append([op1,op2])

	def emit(self,op,dst,src1,src2):
		currScope = self.ST.scopeList[-1]["scope"]
		self.code[currScope]["tac"].append([op,dst,src1,src2])

	
	def emit2(self,op,dst,src1,src2,dst2):
		currScope = self.ST.scopeList[-1]["scope"]
		self.code[currScope]["tac"].append([op,dst,src1,src2,dst2])

	def newLabel(self):
		self.labelCount += 1
		return self.labelPrefix + str(self.labelCount)

	def newTemp(self):
		self.tempCount += 1
		return self.tempPrefix + str(self.tempCount)

	def merge(self, list1, list2):
		list3 = list(list1)+list(list2)
		return list3

	def makeList(self,inputList):
		return [inputList]

	def backpatch(self,listin,value ):
		currScope = self.ST.scopeList[-1]["scope"]
		for quad in listin:
			self.code[currScope]["tac"][quad][-1]=str(value)

	def genFuncTac(self,funcName,startno):
		self.code[funcName] =  {
			"tac":[],
			"startno":startno

		}
	def wrapup(self):
		for quad in self.code[self.ST.scopeList[-1]['scope']]["tac"]:
			if len(quad) == 5 and quad[-1]=="":
				quad[-1]=self.code[self.ST.scopeList[-1]['scope']]["startno"]+len(self.code[self.ST.scopeList[-1]['scope']]["tac"])
		if self.ST.scopeList[-1]["type"]=="function":
			currScope = self.ST.scopeList[-1]["scope"]
			self.code[currScope]["tac"].append(["exit_fun"])


	def printCode(self):
		for quad in (self.code["program"]["tac"]):
			if len(quad) == 1:
				if quad[0] == "exit_fun":
					print "\texit_fun"
				else :
					print quad[0]+":"+"\tbeginFun"
			elif len(quad) == 2:
				if quad[0]=="refparam":
					print "\t"+quad[0]+" "+str(quad[1])
				else:
					print "\tCALL "+quad[0]+" "+ str(quad[1])

			elif len(quad) == 5:
				if quad[0]=="goto" or quad[0]=="return":
					print "\t"+quad[0]+" "+str(quad[-1])
				else :
					print "\t"+"if "+quad[2]+quad[0]+quad[3]+ " goto" + str(quad[-1])
			else:
				if quad[0]=="=" or quad[0]=="+=" or quad[0]=="-=" or quad[0]=="*=" or quad[0]=="/=" or quad[0]=="%=" or quad[0]=="^=" or quad[0]=="&=" or quad[0]=="|=" or quad[0]==">>=" or quad[0]=="<<=" :
					print "\t"+quad[1]+quad[0]+quad[2]+quad[3]
				elif quad[0]=="cast":
					print "\t"+quad[1]+" = "+quad[2]+" ("+quad[3]+") "
				#elif quad[0]=="[]":
				#	print "\t"+quad[1]+" = "+quad[2]+" ["+quad[3]+"] "
				elif quad[-1]=="":
					print "\t"+quad[1]+" = "+quad[0]+" "+quad[2]
				else :
					print "\t"+quad[1]+" = "+quad[2]+quad[0]+quad[3]
		