# README #

### Guidelines about this Repository ###
- A c++ to Mips compiler is to be generated.
- In the current version Lexer and Parser are implemented using PLY package in python.
- To test the lexer and parser use make command to generate executable files.
- Executables for lexer and parser will be generated in bin directory 'lexer' and 'parser' respectively
- The test directory contains 5 test programs named as 'testX.cpp' where, X = 1,2,3,4,5
- To run lexer on any test program type command './bin/lexer /test/testX.cpp' where, X = 1,2,3,4,5
- To run parser on any test program type command './bin/parser /test/testX.cpp' where, X = 1,2,3,4,5
- Parser generates a parse tree as image file 'parse_tree.png'
- make clean command can be used to clean all the binary files and image created

### References ###
- PYDOT package in python is used to draw the parse tree.
- For using stack data structure and various queries PYTHONDS package is used.
- The Grammar for source language is taken from [here](https://www.lysator.liu.se/c/ANSI-C-grammar-y.html)

### Contributers###
- Pranay Borkar(14189)
- Manoj Ghorela(14371)