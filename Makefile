all:
	@cp src/lexer.py bin
	@cp src/parser.py bin
	@cp bin/lexer.py bin/lexer
	@cp bin/parser.py bin/parser
	@cp src/symtab.py bin/symtab.py
	@cp src/tac.py bin/tac.py
	@cp src/codeGen.py bin/codeGen.py
	@chmod +x bin/lexer
	@chmod +x bin/parser
	@echo "----------------------------------------------------------------------------------------------------"
	@echo "|                                        INSTRUCTIONS                                              |"
	@echo "----------------------------------------------------------------------------------------------------"
	@echo "-The bin directory contains the executables for lexer and parser as 'lexer' and 'parser' respectively"
	@echo "-The test directory contains 5 test programs named as 'testX.cpp' where, X = 1,2,3,4,5"
	@echo "-To run lexer on any test program type command './bin/lexer /test/testX.cpp' where, X = 1,2,3,4,5"
	@echo "-To run parser on any test program type command './bin/parser /test/testX.cpp' where, X = 1,2,3,4,5"
	@echo "-Parser generates a parse tree in parse_tree.png"
	@echo ''
lexChange: src/lexer.py
	@cp src/lexer.py bin
	@cp bin/lexer.py bin/lexer
	@chmod +x bin/lexer

parChange: src/parser.py
	@cp src/parser.py bin
	@cp bin/parser.py bin/parser
	@chmod +x bin/parser
clean: 
	@rm bin/*
	@rm ast.png
