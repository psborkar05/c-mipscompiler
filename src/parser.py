#!/usr/bin/env python

import sys
import ply.yacc as yacc
import pydot
from pythonds.basic.stack import Stack
import lexer as lexer
import symtab
import tac
import codeGen
import pprint
pp = pprint.PrettyPrinter(indent=4)

declSpec={}
declrtr={}
scopeFun = False
global_parameter_list=[]
offset = 0
sizeArray = 1
graph = pydot.Dot(graph_type='digraph')
s=Stack()
ST = symtab.symTab()
TAC = tac.ThreeAddressCode(ST)
CG = codeGen.CodeGen(ST,TAC)
# Get the token map

tokens = lexer.tokens

def func( node, stackPops):
    node_par = pydot.Node(node + "" + "_%d" % cnt[node],label=node, style="filled", fillcolor="#976856")
    graph.add_node(node_par)
    for i in range(stackPops):
        node_child = s.pop()
        graph.add_edge(pydot.Edge(node_par, node_child))
    s.push(node_par)
    cnt[node] += 1

literals = [ '+', '-', '*', '/', '%', '^', '&', '|', '~', '!', '<',  '>', '=', ':', '[', ']', '{', '}', '(', ')', '?', '.', ';', ',','<=','>=','<<','>>','!=','==']
cnt = {}
for r in tokens:
    cnt[r] = 0
for r in literals:
    cnt[r] = 0
cnt['EPSILON'] = 0
precedence = (
     ('nonassoc', 'dngElse'),            # Unary minus operator
)

cnt['translation_unit'] = 0
def p_translation_unit_1(p):
    'translation_unit : external_declaration'
    
def p_translation_unit_2(p):
    'translation_unit : translation_unit external_declaration'
    func("translation_unit",2)

# external-declaration:
#def funcheck():
#    ST.addIdentifier('dentifier','idtype',2,2,True)

cnt['external_declaration'] = 0
def p_external_declaration_1(p):
    'external_declaration : function_definition'
    p[0]=p[1]
#    ST.addNewScope( p[0]['declarator']['identifier'], p[0]['declaration_specifiers']['type_specifier'][0],p[0]['declarator']['parameter_type_list'])
#    for declaration in p[0]['compound_statement']['declaration_list']:
#        for decl in declaration['init_declarator_list']:
#            if decl['declarator']['arraydegSize'] == 'invalid':
#                print "ERROR: Invalid declaration of array at line", p[1]['lineNo']
#                exit()
#            else :
#                ST.addIdentifier(decl['declarator']['identifier'],declaration['declaration_specifiers']['type_specifier'][0],len(decl['declarator']['arraydegSize']),sum(map(int,decl['declarator']['arraydegSize'])),decl['declarator']['pointer'])
#    for stmt in p[0]['compound_statement']['statement_list']:
#        for i in stmt['idList']:
#            if ST.lookup(i)==None :
#                print "Error: Variable ",i, " at line ",stmt['lineNo'] ," not declared in scope"
#                exit()
#    print ST.lookup("a")
#    funcheck()
#    ST.delScope(-1)

def p_external_declaration_2(p):
    'external_declaration : declaration'
    #'declaration : declaration_specifiers init_declarator_list SEMI'
    p[0]=p[1]
#    for decl in p[0]['init_declarator_list']:
#        if decl['declarator']['arraydegSize'] == 'invalid':
#            print "ERROR: Invalid declaration of array at line", p[1]['lineNo']
#            exit()
#        else :
#            ST.addIdentifier(decl['declarator']['identifier'],p[0]['declaration_specifiers']['type_specifier'][0],len(decl['declarator']['arraydegSize']),sum(map(int,decl['declarator']['arraydegSize'])),decl['declarator']['pointer'])
    

# function-definition:
cnt['function_definition'] = 0
cnt['scope'] = 0
def p_openScope(p):
    'openScope : empty2'
    global scopeFun
    global offset
    global global_parameter_list
    startLineNo = len(TAC.code[ST.scopeList[-1]['scope']]["tac"])+TAC.code[ST.scopeList[-1]['scope']]["startno"];
    if scopeFun :
        ST.addNewScope(declrtr['identifier'],'function',declSpec['type_specifier'][0],global_parameter_list)
        offset = 0
        TAC.genFuncTac(declrtr['identifier'],startLineNo)
        TAC.emit0(declrtr['identifier'])
        for decl in global_parameter_list:
            if len(decl['declaration_specifiers']['type_specifier'])>1:
                print "ERROR: The type Specifier used at line is not defined in our language."
                exit()
            if decl['declaration_specifiers']['type_specifier'][0] == 'struct':
                ST.addIdentifier("____"+decl['declarator']['identifier'],decl['declaration_specifiers']['type_specifier'][0],len(decl['declarator']['arraydegSize']),sum(map(int,decl['declarator']['arraydegSize'])),decl['declarator']['pointer'])
                ST.addAttrId("____"+decl['declarator']['identifier'], "structName", structName)
            else:
                ST.addIdentifier(decl['declarator']['identifier'],decl['declaration_specifiers']['type_specifier'][0],len(decl['declarator']['arraydegSize']),sum(map(int,decl['declarator']['arraydegSize'])),decl['declarator']['pointer'],offset)
                #print decl['declaration_specifiers']['type_specifier'][0]
                offset = offset + ST.getSize(decl['declaration_specifiers']['type_specifier'][0])
        global_parameter_list=[]
    else:
        ST.addNewScope("scope" + "_%d" % cnt['scope'],'','',[])
        TAC.genFuncTac("scope" + "_%d" % cnt['scope'],startLineNo)
        cnt['scope'] += 1
    scopeFun = False

def p_openStructScope(p):
    'openStructScope : empty2'
    ST.addNewScope("____"+structName,'struct','',[])
    
#first two functions are kind of redundant
def p_function_definition_1(p):
    'function_definition : declaration_specifiers declarator declaration_list compound_statement'
    func("function_definition",4)
    p[0]={
        'declaration_specifiers':p[1],
        'declarator'            :p[2],
        'declaration_list'      :p[3],
        'compound_statement'    :p[4]
    }
def p_function_definition_2(p):
    'function_definition : declarator declaration_list compound_statement'
    func("function_definition",3)
    p[0]={
        'declaration_specifiers':{},
        'declarator'            :p[1],
        'declaration_list'      :p[2],
        'compound_statement'    :p[3]
    }

def p_function_definition_3(p):
    'function_definition : declarator compound_statement'
    func("function_definition",2)
    p[0]={
        'declaration_specifiers':{},
        'declarator'            :p[1],
        'declaration_list'      :[],
        'compound_statement'    :p[2]
    }

def p_function_definition_4(p):
    'function_definition : declaration_specifiers declarator compound_statement'
    func("function_definition",3)
    p[0]={
        'declaration_specifiers':p[1],
        'declarator'            :p[2],
        'declaration_list'      :[],
        'compound_statement'    :p[3]
    }
# declaration:
cnt['declaration'] = 0
def p_declaration_1(p):
    'declaration : declaration_specifiers init_declarator_list SEMI'
    p[0]={
        'declaration_specifiers' : p[1],
        'init_declarator_list'  : p[2],
        'lineNo'    : p.lineno(3)
    }
    global offset
    global sizeArray
    func("declaration",2)
    if len(p[0]['declaration_specifiers']['type_specifier'])>1:
        print "ERROR: The type Specifier used at line",p.lineno(3),"is not defined in our language."
        exit()
    if (p[0]['declaration_specifiers']['type_specifier'][0] == "struct"):
        temp = ST.lookup("____"+structName)
        if temp == None :
            print "No structure named ",structName," defined in the scope"
            exit()
        for decl in p[2]:
            ST.addIdentifier("____"+decl['declarator']['identifier'],p[0]['declaration_specifiers']['type_specifier'][0],len(decl['declarator']['arraydegSize']),sum(map(int,decl['declarator']['arraydegSize'])),decl['declarator']['pointer'],offset)
            if sum(map(int,decl['declarator']['arraydegSize'])) != 0:
                offset = offset + sum(map(int,decl['declarator']['arraydegSize']))*ST.getSize(p[0]['declaration_specifiers']['type_specifier'][0])
            else:
                offset = offset + ST.getSize(p[0]['declaration_specifiers']['type_specifier'][0])
            sizeArray = 1
            ST.addAttrId("____"+decl['declarator']['identifier'], "structName", structName)
    else:
        for decl in p[2]:
            ST.addIdentifier(decl['declarator']['identifier'],p[0]['declaration_specifiers']['type_specifier'][0],len(decl['declarator']['arraydegSize']),sum(map(int,decl['declarator']['arraydegSize'])),decl['declarator']['pointer'],offset)
            ST.addAttrId(decl['declarator']['identifier'],'value',decl['value'])
            if sum(map(int,decl['declarator']['arraydegSize'])) != 0:
                offset = offset + sum(map(int,decl['declarator']['arraydegSize']))*ST.getSize(p[0]['declaration_specifiers']['type_specifier'][0])
            else:
                offset = offset + ST.getSize(p[0]['declaration_specifiers']['type_specifier'][0])
            sizeArray = 1

def p_declaration_2(p):
    'declaration : declaration_specifiers SEMI'

# declaration-list:
cnt['declaration_list'] = 0
cnt['others'] = 0

def p_declaration_list_1(p):
    'declaration_list : declaration'
    p[0]=[p[1]]


def p_declaration_list_2(p):
    'declaration_list : declaration_list declaration '
    func("declaration_list",2)
    p[0]=p[1]
    p[0].append(p[2])


# declaration-specifiers
cnt['declaration_specifiers'] = 0

def p_declaration_specifiers_1(p):
    'declaration_specifiers : storage_class_specifier declaration_specifiers'
    global declSpec
    func("declaration_specifiers",2)
    p[0]=p[2]
    p[0]["storage_class_specifier"].append(p[1])
    declSpec = p[0]

def p_declaration_specifiers_2(p):
    'declaration_specifiers : type_specifier declaration_specifiers'
    global declSpec
    func("declaration_specifiers",2)
    p[0]=p[2]
    p[0]["type_specifier"].append(p[1])
    declSpec = p[0]
    

def p_declaration_specifiers_3(p):
    'declaration_specifiers : type_qualifier declaration_specifiers'
    global declSpec
    func("declaration_specifiers",2)
    p[0]=p[2]
    p[0]["type_qualifier"].append(p[1])
    declSpec = p[0]
    
def p_declaration_specifiers_4(p):
    'declaration_specifiers : storage_class_specifier'
    global declSpec
    p[0]={
        "storage_class_specifier":[p[1]],
        "type_specifier":[],
        "type_qualifier":[]
    }
    declSpec = p[0]
    
def p_declaration_specifiers_5(p):
    'declaration_specifiers : type_specifier'
    global declSpec
    p[0]={
        "storage_class_specifier":[],
        "type_specifier":[p[1]],
        "type_qualifier":[]

    }
    declSpec = p[0]
    
def p_declaration_specifiers_6(p):
    'declaration_specifiers : type_qualifier'
    global declSpec
    p[0]={
        "storage_class_specifier":[],
        "type_specifier":[],
        "type_qualifier":[p[1]]

    }
    declSpec = p[0]
    
# storage-class-specifier
cnt['storage_class_specifier'] = 0

def p_storage_class_specifier_1(p):
    '''storage_class_specifier : AUTO'''
    node = pydot.Node("AUTO" + "_%d" % cnt['AUTO'],label="AUTO", style="filled", fillcolor="#976856")
    graph.add_node(node)
    s.push(node)
    cnt['AUTO'] += 1
    p[0] = p[1]
def p_storage_class_specifier_2(p):
    '''storage_class_specifier : TYPEDEF'''
    node = pydot.Node("TYPEDEF" + "_%d" % cnt['TYPEDEF'],label="TYPEDEF", style="filled", fillcolor="#976856")
    graph.add_node(node)
    s.push(node)
    cnt['TYPEDEF'] += 1
    p[0] = p[1]

def p_storage_class_specifier_3(p):
    '''storage_class_specifier : EXTERN'''
    node = pydot.Node("others" + "_%d" % cnt['others'],label="EXTERN", style="filled", fillcolor="#976856")
    cnt['others'] += 1
    graph.add_node(node)
    s.push(node)
    p[0] = p[1]


def p_storage_class_specifier_4(p):
    '''storage_class_specifier : STATIC'''
    node = pydot.Node("STATIC" + "_%d" % cnt['STATIC'],label="STATIC", style="filled", fillcolor="#976856")
    graph.add_node(node)
    s.push(node)
    cnt['STATIC'] += 1
    p[0] = p[1]

def p_storage_class_specifier_5(p):
    '''storage_class_specifier : REGISTER'''
    node = pydot.Node("REGISTER" + "_%d" % cnt['REGISTER'],label="REGISTER", style="filled", fillcolor="#976856")
    graph.add_node(node)
    s.push(node)
    cnt['REGISTER'] += 1
    p[0] = p[1]
# type-specifier:
cnt['type_specifier'] = 0

def p_type_specifier_1(p):
    '''type_specifier : VOID'''
    node = pydot.Node("VOID" + "_%d" % cnt['VOID'],label="VOID", style="filled", fillcolor="#976856")
    graph.add_node(node)
    s.push(node)
    cnt['VOID'] += 1
    p[0] = p[1]

def p_type_specifier_2(p):
    '''type_specifier : CHAR'''
    node = pydot.Node("CHAR" + "_%d" % cnt['CHAR'],label="CHAR", style="filled", fillcolor="#976856")
    graph.add_node(node)
    s.push(node)
    cnt['CHAR'] += 1
    p[0] = p[1]

def p_type_specifier_3(p):
    '''type_specifier : SHORT'''
    node = pydot.Node("SHORT" + "_%d" % cnt['SHORT'],label="SHORT", style="filled", fillcolor="#976856")
    graph.add_node(node)
    s.push(node)
    cnt['SHORT'] += 1
    p[0] = p[1]

def p_type_specifier_4(p):
    '''type_specifier : INT'''
    node = pydot.Node("INT" + "_%d" % cnt['INT'],label="INT", style="filled", fillcolor="#976856")
    graph.add_node(node)
    s.push(node)
    cnt['INT'] += 1
    p[0] = p[1]

def p_type_specifier_5(p):
    '''type_specifier : LONG'''
    node = pydot.Node("LONG" + "_%d" % cnt['LONG'],label="LONG", style="filled", fillcolor="#976856")
    graph.add_node(node)
    s.push(node)
    cnt['LONG'] += 1
    p[0] = p[1]

def p_type_specifier_6(p):
    '''type_specifier : FLOAT'''
    node = pydot.Node("FLOAT" + "_%d" % cnt['FLOAT'],label="FLOAT", style="filled", fillcolor="#976856")
    graph.add_node(node)
    s.push(node)
    cnt['FLOAT'] += 1
    p[0] = p[1]

def p_type_specifier_7(p):
    '''type_specifier : SIGNED'''
    node = pydot.Node("SIGNED" + "_%d" % cnt['SIGNED'],label="SIGNED", style="filled", fillcolor="#976856")
    graph.add_node(node)
    s.push(node)
    cnt['SIGNED'] +=1
    p[0] = p[1]

def p_type_specifier_8(p):
    '''type_specifier : UNSIGNED'''
    node = pydot.Node("UNSIGNED" + "_%d" % cnt['UNSIGNED'],label="UNSIGNED", style="filled", fillcolor="#976856")
    graph.add_node(node)
    s.push(node)
    cnt['UNSIGNED'] += 1
    p[0] = p[1]

def p_type_specifier_9(p):
    '''type_specifier : TYPE_NAME'''
    node = pydot.Node("TYPE_NAME" + "_%d" % cnt['TYPE_NAME'],label="TYPE_NAME", style="filled", fillcolor="#976856")
    graph.add_node(node)
    s.push(node)
    cnt['TYPE_NAME'] += 1
    p[0] = p[1]

def p_type_specifier_10(p):
    '''type_specifier : struct_or_union_specifier'''
    p[0] = 'struct'

def p_type_specifier_11(p):
    '''type_specifier : enum_specifier'''
    p[0] = 'enum'

def p_type_specifier_12(p):
    '''type_specifier : DOUBLE'''
    node = pydot.Node("DOUBLE" + "_%d" % cnt['DOUBLE'],label="DOUBLE", style="filled", fillcolor="#976856")
    graph.add_node(node)
    s.push(node)
    cnt['DOUBLE'] += 1
    p[0] = p[1]

# type-qualifier:
cnt['type_qualifier'] = 0

def p_type_qualifier_1(p):
    '''type_qualifier : CONST'''
    node = pydot.Node("CONST%d" % cnt['CONST'],label="CONST", style="filled", fillcolor="#976856")
    graph.add_node(node)
    s.push(node)
    cnt['CONST'] += 1
    p[0] = p[1]
def p_type_qualifier_2(p):
    '''type_qualifier : VOLATILE'''
    node = pydot.Node("VOLATILE" + "_%d" % cnt['VOLATILE'],label="VOLATILE", style="filled", fillcolor="#976856")
    graph.add_node(node)
    s.push(node)
    cnt['VOLATILE'] += 1
    p[0] = p[1]

# struct-or-union-specifier
cnt['struct_or_union_specifier'] = 0

def p_struct_or_union_specifier_1(p):
    'struct_or_union_specifier : struct_or_union strctName LBRACE openStructScope struct_declaration_list RBRACE'
    global structName
    node = pydot.Node("IDENTIFIER" + "_%d" % cnt['IDENTIFIER'],label=p[2], style="filled", fillcolor="#976856")
    graph.add_node(node)
    cnt['IDENTIFIER']+=1
    s.push(node)
    func("struct_or_union_specifier",3)
    TAC.wrapup()
    TAC.code[ST.scopeList[-1]['pscope']]["tac"] = TAC.code[ST.scopeList[-1]['pscope']]["tac"]+TAC.code[ST.scopeList[-1]['scope']]["tac"]
    
    ST.addAttrScope("size", offset)    
    temp = ST.scopeList[-1]['scope']
    ST.delScope(-1)
    ST.scopeList[-1][temp]['size'] = offset
    structName = ''

def p_struct_or_union_specifier_2(p):
    'struct_or_union_specifier : struct_or_union LBRACE openStructScope struct_declaration_list RBRACE'
    global structName
    func("struct_or_union_specifier",2)
    TAC.wrapup()
    TAC.code[ST.scopeList[-1]['pscope']]["tac"] = TAC.code[ST.scopeList[-1]['pscope']]["tac"]+TAC.code[ST.scopeList[-1]['scope']]["tac"]
    
    ST.addAttrScope("size", offset)    

    temp = ST.scopeList[-1]['scope']
    ST.delScope(-1)
    ST.scopeList[-1][temp]['size'] = offset
    structName = ''

def p_struct_or_union_specifier_3(p):
    'struct_or_union_specifier : struct_or_union strctName'
    global structName
    node = pydot.Node("IDENTIFIER" + "_%d" % cnt['IDENTIFIER'],label=p[2], style="filled", fillcolor="#976856")
    graph.add_node(node)
    cnt['IDENTIFIER']+=1
    s.push(node)
    func("struct_or_union_specifier",2)
    
def p_strctName(p):
    'strctName : IDENTIFIER'
    global structName
    p[0]=p[1]
    structName = p[1]

# struct-or-union:
cnt['struct_or_union'] = 0

def p_struct_or_union_1(p):
    '''struct_or_union : STRUCT'''
    node = pydot.Node("others" + "_%d" % cnt['others'],label="STRUCT", style="filled", fillcolor="#976856")
    graph.add_node(node)
    cnt['others'] += 1
    s.push(node)
    p[0] = p[1]

def p_struct_or_union_2(p):
    '''struct_or_union : UNION'''
    node = pydot.Node("others" + "_%d" % cnt['others'],label="UNION", style="filled", fillcolor="#976856")
    graph.add_node(node)
    cnt['others'] += 1
    s.push(node)
    p[0] = p[1]

# struct-declaration-list:
cnt['struct_declaration_list'] = 0

def p_struct_declaration_list_1(p):
    'struct_declaration_list : struct_declaration'
    p[0]=[p[1]]


def p_struct_declaration_list_2(p):
    'struct_declaration_list : struct_declaration_list struct_declaration'
    func("struct_declaration_list",2)
    p[0]=p[1]
    p[0].append(p[2])

# init-declarator-list:
cnt['init_declarator_list'] = 0

def p_init_declarator_list_1(p):
    'init_declarator_list : init_declarator'
    p[0]=[p[1]]
    
def p_init_declarator_list_2(p):
    'init_declarator_list : init_declarator_list COMMA init_declarator'
    func("init_declarator_list",2)
    p[0]=p[1]
    p[0].append(p[3])
    
# init-declarator
cnt['init_declarator'] = 0

def p_init_declarator_1(p):
    'init_declarator : declarator'
    p[0]={
    "declarator":p[1],
    "value":''
    }

def p_init_declarator_2(p):
    'init_declarator : declarator EQUALS initializer'
    func("=",2)
    p[0]={
    "declarator":p[1],
    "value":p[3]['value']
    }

# struct-declaration:
cnt['struct_declaration'] = 0

def p_struct_declaration(p):
    'struct_declaration : specifier_qualifier_list struct_declarator_list SEMI'
    func("struct_declaration",2)
    p[0] = {
    "struct_no":cnt['struct_declaration'],
    "specifier_qualifier_list":p[1],
    "struct_declarator_list":p[2]
    }
    global offset
    global sizeArray
    for decl in p[2]:
        if p[1]['type_specifier'] == 'struct':
            ST.addIdentifier("____"+decl['identifier'],p[1]['type_specifier'],len(decl['arraydegSize']),sum(map(int,decl['arraydegSize'])),decl['pointer'],offset)
            if sum(map(int,decl['declarator']['arraydegSize'])) != 0:
                offset = offset + sum(map(int,decl['declarator']['arraydegSize']))*ST.getSize(p[1]['type_specifier'])
            else:
                offset = offset + ST.getSize(p[1]['type_specifier'])
            sizeArray = 1
            ST.addAttrId("____"+decl['identifier'], "structName", structName)
        else:
            ST.addIdentifier(decl['identifier'],p[1]['type_specifier'],len(decl['arraydegSize']),sum(map(int,decl['arraydegSize'])),decl['pointer'],offset)
            if sum(map(int,decl['declarator']['arraydegSize'])) != 0:
                offset = offset + sum(map(int,decl['declarator']['arraydegSize']))*ST.getSize(p[1]['type_specifier'])
            else:
                offset = offset + ST.getSize(p[1]['type_specifier'])
            sizeArray = 1

        cnt['struct_declaration'] += 1


# specifier-qualifier-list:
cnt['specifier_qualifier_list'] = 0

def p_specifier_qualifier_list_1(p):
    'specifier_qualifier_list : type_specifier specifier_qualifier_list'
    func("specifier_qualifier_list",2)
    p[0] = p[2]
    p[0]['type_specifier'].append(p[1])

def p_specifier_qualifier_list_2(p):
    'specifier_qualifier_list : type_specifier'
    p[0] = {
        'type_qualifier':'',
        'type_specifier':p[1]
    }

def p_specifier_qualifier_list_3(p):
    'specifier_qualifier_list : type_qualifier specifier_qualifier_list'
    func("specifier_qualifier_list",2)
    p[0] = p[2]
    p[0]['type_qualifier'].append(p[1])


def p_specifier_qualifier_list_4(p):
    'specifier_qualifier_list : type_qualifier'
    p[0] = {
        'type_qualifier':p[1],
        'type_specifier':''
    }

# struct-declarator-list:
cnt['struct_declarator_list'] = 0


def p_struct_declarator_list_1(p):
    'struct_declarator_list : struct_declarator'
    p[0] = [p[1]]


def p_struct_declarator_list_2(p):
    'struct_declarator_list : struct_declarator_list COMMA struct_declarator'
    func("struct_declarator_list",2)
    p[0] = p[1]
    p[0].append(p[3])

# struct-declarator:
cnt['struct_declarator'] = 0


def p_struct_declarator_1(p):
    'struct_declarator : declarator'
    p[0] = p[1]


def p_struct_declarator_2(p):
    'struct_declarator : declarator COLON constant_expression'
    node = pydot.Node("others" + "_%d" % cnt['others'],label="COLON", style="filled", fillcolor="#976856")
    graph.add_node(node)
    cnt['others'] += 1
    s.push(node)
    func("struct_declarator",3)
    print "The Struct declarator at line",p.lineno(2),"is not defined in our language"
    exit()

def p_struct_declarator_3(p):
    'struct_declarator : COLON constant_expression'
    node = pydot.Node("others" + "_%d" % cnt['others'],label="COLON", style="filled", fillcolor="#976856")
    graph.add_node(node)
    cnt['others'] += 1
    s.push(node)
    func("struct_declarator",2)
    print "The Struct declarator at line",p.lineno(1),"is not defined in our language"
    exit()
# enum-specifier:
cnt['enum_specifier'] = 0

def p_enum_specifier_1(p):
    'enum_specifier : ENUM IDENTIFIER LBRACE openScope enumerator_list RBRACE'
    node = pydot.Node("others" + "_%d" % cnt['others'],label="ENUM", style="filled", fillcolor="#976856")
    graph.add_node(node)
    cnt['others'] += 1
    s.push(node)
    node = pydot.Node("IDENTIFIER" + "_%d" % cnt['IDENTIFIER'],label=p[2], style="filled", fillcolor="#976856")
    graph.add_node(node)
    cnt['IDENTIFIER']+=1
    s.push(node)
    cnt['IDENTIFIER']+=1
    func("enum_specifier",3)
    TAC.wrapup()
    TAC.code[ST.scopeList[-1]['pscope']]["tac"] = TAC.code[ST.scopeList[-1]['pscope']]["tac"]+TAC.code[ST.scopeList[-1]['scope']]["tac"]
    
    ST.addAttrScope("size", offset)    

    temp = ST.scopeList[-1]['scope']
    ST.delScope(-1)
    ST.scopeList[-1][temp]['size'] = offset
    

def p_enum_specifier_2(p):
    'enum_specifier : ENUM LBRACE openScope enumerator_list RBRACE'
    node = pydot.Node("others" + "_%d" % cnt['others'],label="ENUM", style="filled", fillcolor="#976856")
    graph.add_node(node)
    cnt['others'] += 1
    s.push(node)
    func("enum_specifier",2)
    TAC.wrapup()
    TAC.code[ST.scopeList[-1]['pscope']]["tac"] = TAC.code[ST.scopeList[-1]['pscope']]["tac"]+TAC.code[ST.scopeList[-1]['scope']]["tac"]
    
    ST.addAttrScope("size", offset)    

    temp = ST.scopeList[-1]['scope']
    ST.delScope(-1)
    ST.scopeList[-1][temp]['size'] = offset
    

def p_enum_specifier_3(p):
    'enum_specifier : ENUM IDENTIFIER'
    node = pydot.Node("others" + "_%d" % cnt['others'],label="ENUM", style="filled", fillcolor="#976856")
    graph.add_node(node)
    cnt['others'] += 1
    s.push(node)
    node = pydot.Node("IDENTIFIER" + "_%d" % cnt['IDENTIFIER'],label=p[2], style="filled", fillcolor="#976856")
    graph.add_node(node)
    cnt['IDENTIFIER']+=1
    s.push(node)
    func("enum_specifier",2)

# enumerator_list:
cnt['enum_specifier'] = 0

def p_enumerator_list_1(p):
    'enumerator_list : enumerator'


def p_enumerator_list_2(p):
    'enumerator_list : enumerator_list COMMA enumerator'
    func("enumerator_list",2)

# enumerator:
cnt['enumerator'] = 0

def p_enumerator_1(p):
    'enumerator : IDENTIFIER'
    node = pydot.Node("IDENTIFIER" + "_%d" % cnt['IDENTIFIER'],label=p[1], style="filled", fillcolor="#976856")
    graph.add_node(node)
    cnt['IDENTIFIER']+=1
    s.push(node)
    
def p_enumerator_2(p):
    'enumerator : IDENTIFIER EQUALS constant_expression'
    node = pydot.Node("IDENTIFIER" + "_%d" % cnt['IDENTIFIER'],label=p[1], style="filled", fillcolor="#976856")
    graph.add_node(node)
    cnt['IDENTIFIER']+=1
    s.push(node)
    func("=",2)

# declarator:
cnt['declarator'] = 0


def p_declarator_1(p):
    'declarator : pointer direct_declarator'
    global declrtr 
    func("declarator",2)
    p[0] = p[2]
    p[0]['pointer'] = True 
    declrtr = p[0]

def p_declarator_2(p):
    'declarator : direct_declarator'
    global declrtr 
    p[0] = p[1]
    p[0]['pointer'] = False
    declrtr = p[0]

# direct-declarator:
cnt['direct_declarator'] = 0

def p_direct_declarator_1(p):
    'direct_declarator : IDENTIFIER'
    node = pydot.Node("IDENTIFIER" + "_%d" % cnt['IDENTIFIER'],label=p[1], style="filled", fillcolor="#976856")
    graph.add_node(node)
    cnt['IDENTIFIER']+=1
    s.push(node)
    p[0]={
        'identifier':p[1],
        'arraydegSize':[],
        'parameter_type_list':'invalid'
    }

def p_direct_declarator_2(p):
    'direct_declarator : LPAREN declarator RPAREN'
    #p[0]=p[1]+p[2]+p[3]
    p[0]={
        'identifier':p[1],
        'arraydegSize':p[2]['arraydegSize'],
        'parameter_type_list': p[0]['parameter_type_list']
    }


def p_direct_declarator_3(p):
    'direct_declarator : direct_declarator LBRACKET constant_expression_opt RBRACKET'
    func("direct_declarator",2)
    #p[0]=p[1]+p[2]+p[3]+p[4]
    #p[0]['declarator'] = p[1]['declarator']+'['+'constant_expression_opt' + ']'
    p[0]=p[1]
#    print(p[3]['type'])
    if p[3]['type']=="int" and isinstance(p[0]['arraydegSize'],list):
        p[0]['arraydegSize'].append(p[3]["value"])
    else :
        p[0]['arraydegSize']='invalid'
    p[0]['parameter_type_list']='invalid'


def p_direct_declarator_4(p):
    'direct_declarator : direct_declarator LPAREN parameter_type_list RPAREN '
    func("direct_declarator",2)
    #p[0]['declarator'] = p[1]['declarator']+ '(' +'parameter_type_list' + ')'
    p[0]=p[1]

    p[0]['arraydegSize']='invalid'
    p[0]['parameter_type_list']=p[3]
    global scopeFun
    scopeFun = True

def p_direct_declarator_5(p):
    'direct_declarator : direct_declarator LPAREN identifier_list RPAREN '
    func("direct_declarator",2)
    #p[0]['declarator'] = p[1]['declarator']+ '(' +'identifier_list' + ')'
    p[0]=p[1]
    p[0]['arraydegSize']='invalid'
    p[0]['parameter_type_list']='invalid'

def p_direct_declarator_6(p):
    'direct_declarator : direct_declarator LPAREN RPAREN '
    #p[0]['declarator'] = p[1]['declarator'] + '()'
    p[0]=p[1]
    p[0]['arraydegSize']='invalid'
    p[0]['parameter_type_list']=[]
    global scopeFun
    scopeFun = True
    
# pointer:
cnt['pointer'] = 0


def p_pointer_1(p):
    'pointer : TIMES type_qualifier_list'
    node = pydot.Node("others" + "_%d" % cnt['others'],label="TIMES", style="filled", fillcolor="#976856")
    graph.add_node(node)
    cnt['others'] += 1
    s.push(node)
    func("pointer",2)
    p[0]= p[1] + ''.join(p[2])

def p_pointer_2(p):
    'pointer : TIMES'
    node = pydot.Node("others" + "_%d" % cnt['others'],label="TIMES", style="filled", fillcolor="#976856")
    graph.add_node(node)
    cnt['others'] += 1
    s.push(node)
    #p[0]['pointer']= '*'
    p[0]=p[1]


def p_pointer_3(p):
    'pointer : TIMES type_qualifier_list pointer'
    node = pydot.Node("others" + "_%d" % cnt['others'],label="TIMES", style="filled", fillcolor="#976856")
    graph.add_node(node)
    cnt['others'] += 1
    s.push(node)
    func("pointer",3)
    #p[0]['pointer']= '*' + p[2]['pointer'] + p[3]['pointer']
    p[0]= p[1] + ''.join(p[2])+p[3]



def p_pointer_4(p):
    'pointer : TIMES pointer'
    node = pydot.Node("others" + "_%d" % cnt['others'],label="TIMES", style="filled", fillcolor="#976856")
    graph.add_node(node)
    cnt['others'] += 1
    s.push(node)
    func("pointer",2)
    #p[0]['pointer']= '*' + p[2]['pointer']
    p[0]= p[1] + p[2]


# type-qualifier-list:
cnt['type_qualifier_list'] = 0

def p_type_qualifier_list_1(p):
    'type_qualifier_list : type_qualifier'
    p[0]= [p[1]]

def p_type_qualifier_list_2(p):
    'type_qualifier_list : type_qualifier_list type_qualifier'
    func("type_qualifier_list",2)
    p[0] = p[1]
    p[0].append(p[2])

# parameter-type-list:
cnt['parameter_type_list'] = 0


def p_parameter_type_list_1(p):
    'parameter_type_list : parameter_list'
    global global_parameter_list
    global_parameter_list = p[1]
    p[0]=p[1]


def p_parameter_type_list_2(p):
    'parameter_type_list : parameter_list COMMA ELLIPSIS'
    node = pydot.Node("others" + "_%d" % cnt['others'],label="ELLIPSIS", style="filled", fillcolor="#976856")
    graph.add_node(node)
    cnt['others'] += 1
    s.push(node)
    func("parameter_type_list",2)
    global global_parameter_list
    global_parameter_list = p[1]
    p[0]=p[1]
#    p[0].append(p[3])

# parameter-list:
cnt['parameter_list'] = 0

def p_parameter_list_1(p):
    'parameter_list : parameter_declaration'
    p[0]=[p[1]]

def p_parameter_list_2(p):
    'parameter_list : parameter_list COMMA parameter_declaration'
    func("parameter_list",2)
    p[0]=p[1]
    p[0].append(p[3])

# parameter-declaration:
cnt['parameter_declaration'] = 0

def p_parameter_declaration_1(p):
    'parameter_declaration : declaration_specifiers declarator'
    func("parameter_declaration",2)
    p[0]={
        'declaration_specifiers':p[1],
        'declarator'    :p[2],
        'size':ST.getSize(p[1]['type_specifier'][-1])
    }
    

def p_parameter_declaration_2(p):
    'parameter_declaration : declaration_specifiers abstract_declarator_opt'
    func("parameter_declaration",2)
    p[0]={
        'declaration_specifiers':p[1],
        'declarator'    :'invalid',
        'size':ST.getSize(p[1]['type_specifier'][-1])
    }
# identifier-list:
cnt['identifier_list'] = 0

def p_identifier_list_1(p):
    'identifier_list : IDENTIFIER'
    node = pydot.Node("IDENTIFIER" + "_%d" % cnt['IDENTIFIER'],label=p[1], style="filled", fillcolor="#976856")
    graph.add_node(node)
    cnt['IDENTIFIER']+=1
    s.push(node)
    p[0]=[p[1]]


def p_identifier_list_2(p):
    'identifier_list : identifier_list COMMA IDENTIFIER'
    node = pydot.Node("IDENTIFIER" + "_%d" % cnt['IDENTIFIER'],label=p[3], style="filled", fillcolor="#976856")
    graph.add_node(node)
    cnt['IDENTIFIER']+=1
    s.push(node)
    func("identifier_list",2)
    p[0]=p[1]
    p[0].append(p[3])

# initializer:
cnt['initializer'] = 0

def p_initializer_1(p):
    'initializer : assignment_expression'
    p[0]=p[1]

def p_initializer_2(p):
    '''initializer : LBRACE openScope initializer_list RBRACE'''
    p[0]=p[1]+''.join(p[2])+p[3]
    TAC.wrapup()
    TAC.code[ST.scopeList[-1]['pscope']]["tac"] = TAC.code[ST.scopeList[-1]['pscope']]["tac"]+TAC.code[ST.scopeList[-1]['scope']]["tac"]
    
    ST.addAttrScope("size", offset)    

    temp = ST.scopeList[-1]['scope']
    ST.delScope(-1)
    ST.scopeList[-1][temp]['size'] = offset
    
def p_initializer_3(p):
    '''initializer : LBRACE openScope initializer_list COMMA RBRACE '''
    p[0]=p[1]+''.join(p[2])+p[3]+p[4]
    TAC.wrapup()
    TAC.code[ST.scopeList[-1]['pscope']]["tac"] = TAC.code[ST.scopeList[-1]['pscope']]["tac"]+TAC.code[ST.scopeList[-1]['scope']]["tac"]
    
    ST.addAttrScope("size", offset)    
    temp = ST.scopeList[-1]['scope']
    ST.delScope(-1)
    ST.scopeList[-1][temp]['size'] = offset
    
# initializer-list:
cnt['initializer_list'] = 0


def p_initializer_list_1(p):
    'initializer_list : initializer'
    p[0]=[p[1]]


def p_initializer_list_2(p):
    'initializer_list : initializer_list COMMA initializer'
    func("initializer_list",2)
    p[0]=p[1]
    p[0].append(p[3])

# type-name:
cnt['type_name'] = 0

def p_type_name(p):
    'type_name : specifier_qualifier_list abstract_declarator_opt'
    func("type_name",2)
    p[0]={
        'specifier_qualifier_list':p[1],
        'abstract_declarator_opt' :p[2]
    }

# abstract_declarator_opt:
cnt['abstract_declarator_opt'] = 0

def p_abstract_declarator_opt_1(p):
    'abstract_declarator_opt : empty'
    p[0]={
        'pointer':False,
        'identifier':'',
        'arraydegSize':[],
        'parameter_type_list':[]
    
    }

def p_abstract_declarator_opt_2(p):
    'abstract_declarator_opt : abstract_declarator'
    p[0]=p[1]

# abstract-declarator:
cnt['abstract_declarator'] = 0

def p_abstract_declarator_1(p):
    'abstract_declarator : pointer '
    p[0]={
        'pointer':True,
        'identifier':'',
        'arraydegSize':[],
        'parameter_type_list':[]
    
    }

def p_abstract_declarator_2(p):
    'abstract_declarator : pointer direct_abstract_declarator'
    func("abstract_declarator",2)
    p[0]=p[2]
    p[0]['pointer']=True

def p_abstract_declarator_3(p):
    'abstract_declarator : direct_abstract_declarator'
    p[0]=p[1]
    p[0]['pointer']=True

# direct-abstract-declarator:
cnt['direct_abstract_declarator'] = 0


def p_direct_abstract_declarator_1(p):
    'direct_abstract_declarator : LPAREN abstract_declarator RPAREN'
    p[0]=p[2]

def p_direct_abstract_declarator_2(p):
    'direct_abstract_declarator : direct_abstract_declarator LBRACKET constant_expression_opt RBRACKET'
    func("direct_abstract_declarator",2)
    p[0]=p[1]


def p_direct_abstract_declarator_3(p):
    'direct_abstract_declarator : LBRACKET constant_expression_opt RBRACKET'
    p[0]={
        'identifier':'',
        'arraydegSize':[],
        'parameter_type_list':[]
    }

def p_direct_abstract_declarator_4(p):
    'direct_abstract_declarator : direct_abstract_declarator LPAREN parameter_type_list_opt RPAREN'
    func("direct_abstract_declarator",2)
    p[0]=p[1]
    p[0]['parameter_type_list']=p[3]

def p_direct_abstract_declarator_5(p):
    'direct_abstract_declarator : LPAREN parameter_type_list_opt RPAREN'
    p[0]={
        'identifier':'',
        'arraydegSize':[],
        'parameter_type_list':p[2]
    }

# Optional fields in abstract declarators
cnt['constant_expression_opt'] = 0


def p_constant_expression_opt_1(p):
    'constant_expression_opt : empty'
    p[0]= {
        'type':''
    }


def p_constant_expression_opt_2(p):
    'constant_expression_opt : constant_expression'
    p[0]=p[1]
# Optional fields in abstract declarators
cnt['parameter_type_list_opt'] = 0

def p_parameter_type_list_opt_1(p):
    'parameter_type_list_opt : empty'
    p[0]=[]


def p_parameter_type_list_opt_2(p):
    'parameter_type_list_opt : parameter_type_list'
    p[0]=p[1]
# statement:
cnt['statement'] = 0



def p_statement_1(p):
    'statement : labeled_statement'
    p[0]=p[1]
    p[0]['statement_type']='labeled_statement'
def p_statement_2(p):
    'statement : expression_statement'
    p[0]=p[1]
    p[0]['statement_type']='expression_statement'
def p_statement_3(p):
    'statement : compound_statement'
    p[0]=p[1]
    p[0]['statement_type']='compound_statement'
def p_statement_4(p):
    'statement : selection_statement'
    p[0]=p[1]
    p[0]['statement_type']='selection_statement'
def p_statement_5(p):
    'statement : iteration_statement'
    p[0]=p[1]
    p[0]['statement_type']='iteration_statement'
def p_statement_6(p):
    'statement : jump_statement'
    p[0]=p[1]
    p[0]['statement_type']='jump_statement'

# labeled-statement:
cnt['labeled_statement'] = 0


def p_labeled_statement_1(p):
    'labeled_statement : IDENTIFIER COLON statement'
    node = pydot.Node("IDENTIFIER" + "_%d" % cnt['IDENTIFIER'],label=p[1], style="filled", fillcolor="#976856")
    cnt['IDENTIFIER']+=1
    graph.add_node(node)
    s.push(node)
    func(":",2)
    p[0]=p[3]
    temp = ST.lookup(p[1])
    if temp == None :
        print "Variable \'",p[1],"\' at line no ",p.lineno(1)," not declared in the scope"
        exit()

def p_labeled_statement_2(p):
    'labeled_statement : CASE constant_expression COLON statement'
    node = pydot.Node("CASE" + "_%d" % cnt['CASE'],label="CASE", style="filled", fillcolor="#976856")
    cnt['CASE']+=1
    graph.add_node(node)
    s.push(node)
    func(":",3)
    p[0]={
        "nextList":p[4]["nextList"],
        "brkList":p[4]["brkList"],
        "tempvar":TAC.newTemp()
    }

    TAC.emit2("!=",p[0]["tempvar"],p[2]["tempvar"],p[4]["tempvar"],)
def p_labeled_statement_3(p):
    'labeled_statement : DEFAULT COLON statement'
    node = pydot.Node("DEFAULT" + "_%d" % cnt['DEFAULT'],label="DEFAULT", style="filled", fillcolor="#976856")
    cnt['DEFAULT']+=1
    graph.add_node(node)
    s.push(node)
    func(":",2)
    p[0]=p[2]

# expression-statement:
cnt['expression_statement'] = 0

def p_expression_statement(p):
    'expression_statement : expression_opt SEMI'
    p[0]=p[1]
    p[0]['lineNo']=p.lineno(2)
# compound-statement:
cnt['compound_statement'] = 0

def p_compound_statement_1(p):
    'compound_statement : LBRACE openScope declaration_list statement_list RBRACE'
    func("compound_statement",2)

    p[0]={
        'declaration_list'  :p[2],
        'statement_list'    :p[4]["list"],
        "nextList":p[4]["nextList"],
        "brkList":p[4]["brkList"]
 
    }
    p[0]["nextList"] = [x+TAC.code[ST.scopeList[-1]['pscope']]["startno"] for x in p[0]["nextList"]]
    p[0]["brkList"] = [x+TAC.code[ST.scopeList[-1]['pscope']]["startno"] for x in p[0]["brkList"]]
    TAC.wrapup()
    TAC.code[ST.scopeList[-1]['pscope']]["tac"] = TAC.code[ST.scopeList[-1]['pscope']]["tac"]+TAC.code[ST.scopeList[-1]['scope']]["tac"]
    
    ST.addAttrScope("size", offset)    

    temp = ST.scopeList[-1]['scope']
    ST.delScope(-1)
    ST.scopeList[-1][temp]['size'] = offset
    
def p_compound_statement_2(p):
    'compound_statement : LBRACE openScope statement_list RBRACE'
    p[0]={
        'declaration_list'  :[],
        'statement_list'    :p[3]["list"],
        "nextList":p[3]["nextList"],
        "brkList":p[3]["brkList"]
    }
    p[0]["nextList"] = [x+TAC.code[ST.scopeList[-1]['pscope']]["startno"] for x in p[0]["nextList"]]
    p[0]["brkList"] = [x+TAC.code[ST.scopeList[-1]['pscope']]["startno"] for x in p[0]["brkList"]]
    
    TAC.wrapup()
    TAC.code[ST.scopeList[-1]['pscope']]["tac"] = TAC.code[ST.scopeList[-1]['pscope']]["tac"]+TAC.code[ST.scopeList[-1]['scope']]["tac"]
    
    ST.addAttrScope("size", offset)    

    temp = ST.scopeList[-1]['scope']
    ST.delScope(-1)
    ST.scopeList[-1][temp]['size'] = offset
    
def p_compound_statement_3(p):
    'compound_statement : LBRACE openScope declaration_list RBRACE'
    p[0]={
        'declaration_list'  :p[2],
        'statement_list'    :[],
        "nextList":[],
        "brkList":[]
    }
    TAC.wrapup()
    TAC.code[ST.scopeList[-1]['pscope']]["tac"] = TAC.code[ST.scopeList[-1]['pscope']]["tac"]+TAC.code[ST.scopeList[-1]['scope']]["tac"]
    
    ST.addAttrScope("size", offset)    

    temp = ST.scopeList[-1]['scope']
    ST.delScope(-1)
    ST.scopeList[-1][temp]['size'] = offset
    
def p_compound_statement_4(p):
    'compound_statement : LBRACE openScope RBRACE'
    node = pydot.Node("LBRACE" + "_%d" % cnt['LBRACE'],label="LBRACE", style="filled", fillcolor="#976856")
    cnt['LBRACE']+=1
    graph.add_node(node)
    s.push(node)
    node = pydot.Node("RBRACE" + "_%d" % cnt['RBRACE'],label="RBRACE", style="filled", fillcolor="#976856")
    cnt['RBRACE']+=1
    graph.add_node(node)
    s.push(node)
    func("compound_statement",2)
    p[0]={
        'declaration_list'  :[],
        'statement_list'    :[],
        "nextList":[],
        "brkList": []
    }
    TAC.wrapup()
    TAC.code[ST.scopeList[-1]['pscope']]["tac"] = TAC.code[ST.scopeList[-1]['pscope']]["tac"]+TAC.code[ST.scopeList[-1]['scope']]["tac"]
    
    ST.addAttrScope("size", offset)    

    temp = ST.scopeList[-1]['scope']
    ST.delScope(-1)
    ST.scopeList[-1][temp]['size'] = offset
    
# statement-list:
cnt['statement_list'] = 0


def p_statement_list_1(p):
    'statement_list : statement'
    p[0]={
        "list":[p[1]],
        "nextList":p[1]["nextList"],
        "brkList":p[1]["brkList"]
    }


def p_statement_list_2(p):
    'statement_list : statement_list dummyM statement'
    func("statement_list",2)
    TAC.backpatch(p[1]["nextList"],p[2]["quad"]+TAC.code[ST.scopeList[-1]['scope']]["startno"])
    p[0]={
        "list":p[1]["list"],
        "nextList":p[3]["nextList"],
        "brkList":p[1]["brkList"]+p[3]["brkList"]
    }
    p[0]["list"].append(p[2])

# selection-statement
cnt['IF_ELSE'] = 0

def p_selection_statement_1(p):
    'selection_statement : IF LPAREN expression RPAREN dummyM statement dummyN ELSE dummyM statement %prec dngElse'
    stmt2=s.pop()
    stmt1=s.pop()
    expr=s.pop()
    node = "IF_ELSE"
    node_par = pydot.Node(node + "" + "_%d" % cnt[node],label=node, style="filled", fillcolor="#976856")
    graph.add_node(node_par)
    graph.add_edge(pydot.Edge(node_par, expr, label="Condition Statment", labelfontcolor="#009933", fontsize="10.0", color="black"))
    graph.add_edge(pydot.Edge(node_par, stmt1, label="Condition True", labelfontcolor="#009933", fontsize="10.0", color="black"))
    graph.add_edge(pydot.Edge(node_par, stmt2, label="Condition False", labelfontcolor="#009933", fontsize="10.0", color="black"))
    s.push(node_par)
    cnt[node] += 1
    TAC.backpatch(p[3]["truelist"],p[5]["quad"]+TAC.code[ST.scopeList[-1]['scope']]["startno"])
    TAC.backpatch(p[3]["falselist"],p[9]["quad"]+TAC.code[ST.scopeList[-1]['scope']]["startno"])
    p[0]={
        "nextList":p[6]["nextList"]+p[10]["nextList"]+p[7]["nextList"],
        "brkList":p[6]["brkList"]+p[10]["brkList"]
    }
cnt['IF_STMT'] = 0

def p_selection_statement_2(p):
    'selection_statement : IF LPAREN expression RPAREN dummyM statement'
    stmt1=s.pop()
    expr=s.pop()
    node = "IF_STMT"
    node_par = pydot.Node(node + "" + "_%d" % cnt[node],label=node, style="filled", fillcolor="#976856")
    graph.add_node(node_par)
    graph.add_edge(pydot.Edge(node_par, expr, label="Condition Statment", labelfontcolor="#009933", fontsize="10.0", color="black"))
    graph.add_edge(pydot.Edge(node_par, stmt1, label="Condition True", labelfontcolor="#009933", fontsize="10.0", color="black"))
    s.push(node_par)
    cnt[node] += 1
    TAC.backpatch(p[3]["truelist"],p[5]["quad"]+TAC.code[ST.scopeList[-1]['scope']]["startno"])
    p[0]={
        "nextList":p[3]["falselist"]+p[6]["nextList"],
        "brkList":p[6]["brkList"]
    }

def p_selection_statement_3(p):
    'selection_statement : SWITCH LPAREN expression RPAREN statement'
    stmt1=s.pop()
    expr=s.pop()
    node = "SWITCH"
    node_par = pydot.Node(node + "" + "_%d" % cnt[node],label=node, style="filled", fillcolor="#976856")
    graph.add_node(node_par)
    graph.add_edge(pydot.Edge(node_par, expr, label="Expression", labelfontcolor="#009933", fontsize="10.0", color="black"))
    graph.add_edge(pydot.Edge(node_par, stmt1, label="Statement", labelfontcolor="#009933", fontsize="10.0", color="black"))
    s.push(node_par)
    cnt[node] += 1
    p[0]={
        "nextList":p[5]["brkList"],
        "brkList":[]
    }
# iteration_statement:
cnt['iteration_statement'] = 0

def p_iteration_statement_1(p):
    'iteration_statement : WHILE LPAREN dummyM expression RPAREN dummyM statement'
    stmt1=s.pop()
    expr=s.pop()
    node = "WHILE"
    node_par = pydot.Node(node + "" + "_%d" % cnt[node],label=node, style="filled", fillcolor="#976856")
    graph.add_node(node_par)
    graph.add_edge(pydot.Edge(node_par, expr, label="Loop Condition", labelfontcolor="#009933", fontsize="10.0", color="black"))
    graph.add_edge(pydot.Edge(node_par, stmt1, label="Statements", labelfontcolor="#009933", fontsize="10.0", color="black"))
    s.push(node_par)
    cnt[node] += 1
    TAC.backpatch(p[7]["nextList"],p[3]["quad"]+TAC.code[ST.scopeList[-1]['scope']]["startno"])
    TAC.backpatch(p[4]["truelist"],p[6]["quad"]+TAC.code[ST.scopeList[-1]['scope']]["startno"])
    p[0]={
        "nextList":p[4]["falselist"]+p[7]["brkList"],
        "brkList":[]
    }
    TAC.emit2("goto","","","",p[3]['quad']+TAC.code[ST.scopeList[-1]['scope']]["startno"])
    
def p_iteration_statement_2(p):
    'iteration_statement : FOR LPAREN expression_opt SEMI dummyM expression_opt SEMI dummyM expression_opt dummyN RPAREN dummyM statement '
    stmt1=s.pop()
    expr3=s.pop()
    expr2=s.pop()
    expr1=s.pop()
    node = "FOR"
    node_par = pydot.Node(node + "" + "_%d" % cnt[node],label=node, style="filled", fillcolor="#976856")
    graph.add_node(node_par)
    
    graph.add_edge(pydot.Edge(node_par, expr1, label="Loop Var Change", labelfontcolor="#009933", fontsize="10.0", color="black"))
    graph.add_edge(pydot.Edge(node_par, expr2, label="Loop Var Condn", labelfontcolor="#009933", fontsize="10.0", color="black"))
    graph.add_edge(pydot.Edge(node_par, expr3, label="Loop Var Init", labelfontcolor="#009933", fontsize="10.0", color="black"))
    
    graph.add_edge(pydot.Edge(node_par, stmt1, label="Statements", labelfontcolor="#009933", fontsize="10.0", color="black"))
    s.push(node_par)
    cnt[node] += 1
    TAC.backpatch(p[6]["truelist"],p[12]["quad"]+TAC.code[ST.scopeList[-1]['scope']]["startno"])
    TAC.backpatch(p[10]["nextList"],p[5]["quad"]+TAC.code[ST.scopeList[-1]['scope']]["startno"])
    TAC.backpatch(p[13]["nextList"],p[8]["quad"]+TAC.code[ST.scopeList[-1]['scope']]["startno"])
    p[0]={
        "nextList":p[6]["falselist"]+p[13]["brkList"],
        "brkList":[]
    }
    TAC.emit2("goto","","","",p[8]['quad']+TAC.code[ST.scopeList[-1]['scope']]["startno"])
    
cnt['DO_WHILE']=0
def p_iteration_statement_3(p):
    'iteration_statement : DO dummyM statement WHILE LPAREN dummyM expression dummyN RPAREN SEMI'
    expr=s.pop()
    stmt1=s.pop()
    node = "DO_WHILE"
    node_par = pydot.Node(node + "" + "_%d" % cnt[node],label=node, style="filled", fillcolor="#976856")
    graph.add_node(node_par)
    graph.add_edge(pydot.Edge(node_par, expr, label="Loop Condition", labelfontcolor="#009933", fontsize="10.0", color="black"))
    graph.add_edge(pydot.Edge(node_par, stmt1, label="Statements", labelfontcolor="#009933", fontsize="10.0", color="black"))
    s.push(node_par)
    cnt[node] += 1
    func("iteration_statement",4)
    TAC.backpatch(p[7]["truelist"],p[2]["quad"]+TAC.code[ST.scopeList[-1]['scope']]["startno"])
    TAC.backpatch(p[4]["nextList"],p[6]["quad"]+TAC.code[ST.scopeList[-1]['scope']]["startno"])
    TAC.backpatch(p[8]["nextList"],p[2]["quad"]+TAC.code[ST.scopeList[-1]['scope']]["startno"])
    p[0]={
        "nextList":p[7]["falselist"]+p[3]["brkList"],
        "brkList":[]
    }
# jump_statement:
cnt['jump_statement'] = 0

def p_jump_statement_1(p):
    'jump_statement : GOTO IDENTIFIER SEMI'
    node = pydot.Node("GOTO" + "_%d" % cnt['GOTO'],label="GOTO", style="filled", fillcolor="#976856")
    cnt['GOTO'] += 1
    graph.add_node(node)
    s.push(node)
    node = pydot.Node("IDENTIFIER" + "_%d" % cnt['IDENTIFIER'],label=p[2], style="filled", fillcolor="#976856")
    cnt['IDENTIFIER']+=1
    graph.add_node(node)
    s.push(node)
    func("jump_statement",2)
    temp = ST.lookup(p[2])
    if temp == None :
        print "Variable \'",p[2],"\' at line no ",p.lineno(2)," not declared in the scope"
        exit()
    p[0]={
        "nextList":[],
        "brkList":[]
    }
    TAC.emit2("goto","","","",p[2])
def p_jump_statement_2(p):
    'jump_statement : CONTINUE SEMI'
    node = pydot.Node("CONTINUE" + "_%d" % cnt['CONTINUE'],label="CONTINUE", style="filled", fillcolor="#976856")
    cnt['CONTINUE'] += 1
    graph.add_node(node)
    s.push(node)
    p[0]={
        "nextList":[len(TAC.code[ST.scopeList[-1]['scope']]["tac"])],
        "brkList":[]
    }
    TAC.emit2("goto","","","","")

def p_jump_statement_3(p):
    'jump_statement : BREAK SEMI'
    node = pydot.Node("BREAK" + "_%d" % cnt['BREAK'],label="BREAK", style="filled", fillcolor="#976856")
    cnt['BREAK'] += 1
    graph.add_node(node)
    s.push(node)
    p[0]={
        "nextList":[],
        "brkList":[len(TAC.code[ST.scopeList[-1]['scope']]["tac"])]
    }
    TAC.emit2("goto","","","","")

def p_jump_statement_4(p):
    'jump_statement : RETURN expression_opt SEMI'
    node = pydot.Node("RETURN" + "_%d" % cnt['RETURN'],label="RETURN", style="filled", fillcolor="#976856")
    cnt['RETURN'] += 1
    graph.add_node(node)
    s.push(node)
    func("jump_statement",2)
    p[0]={
        "nextList":[],
        "brkList":[]
    }
    TAC.emit2("return","","","",p[2]['tempvar'])

# expression_opt:
cnt['expression_opt'] = 0

def p_expression_opt_1(p):
    'expression_opt : empty'
    p[0]={}
    p[0]["nextList"] = []
    p[0]["brkList"] = []

def p_expression_opt_2(p):
    'expression_opt : expression'
    p[0]=p[1]
    p[0]["nextList"] = []
    p[0]["brkList"] = []
# expression:
cnt['expression'] = 0

def p_expression_1(p):
    'expression : assignment_expression'
    p[0]=p[1]

def p_expression_2(p):
    'expression : expression COMMA assignment_expression'
    func("expression",2)
    p[0]=p[1]
    p[0]['lineNo']=p.lineno(2)
# assigment_expression:
cnt['assignment_expression'] = 0

def p_assignment_expression_1(p):
    'assignment_expression : conditional_expression'
    p[0]=p[1]

def p_assignment_expression_2(p):
    'assignment_expression : unary_expression assignment_operator assignment_expression'
    assiExp = s.pop()
    assiOp = s.pop()
    uniExp = s.pop()
    graph.add_edge(pydot.Edge(assiOp,assiExp))
    graph.add_edge(pydot.Edge(assiOp,uniExp))
    s.push(assiOp)
    p[0]=p[1]
    tempsrc = p[0]['tempvar']
    
    p[0]['lineNo']=p[2]['lineNo']

    TAC.emit(p[2]['operator'],p[1]["tempvar"],p[3]["tempvar"],"")

# assignment_operator:
cnt['assignment_operator'] = 0

def p_assignment_operator_1(p):
    '''assignment_operator : EQUALS'''
    node = pydot.Node("others" + "_%d" % cnt['others'],label=str(p[1]), style="filled", fillcolor="#976856")
    cnt['others'] += 1
    graph.add_node(node)
    s.push(node)
    p[0]={
        'operator':p[1],
        'lineNo':p.lineno(1)
    }
def p_assignment_operator_2(p):
    '''assignment_operator : MUL_ASSIGN'''
    node = pydot.Node("others" + "_%d" % cnt['others'],label=str(p[1]), style="filled", fillcolor="#976856")
    cnt['others'] += 1
    graph.add_node(node)
    s.push(node)
    p[0]={
        'operator':p[1],
        'lineNo':p.lineno(1)
    }
def p_assignment_operator_3(p):
    '''assignment_operator : DIV_ASSIGN'''
    node = pydot.Node("others" + "_%d" % cnt['others'],label=str(p[1]), style="filled", fillcolor="#976856")
    cnt['others'] += 1
    graph.add_node(node)
    s.push(node)
    p[0]={
        'operator':p[1],
        'lineNo':p.lineno(1)
    }
def p_assignment_operator_4(p):
    '''assignment_operator : MOD_ASSIGN'''
    node = pydot.Node("others" + "_%d" % cnt['others'],label=str(p[1]), style="filled", fillcolor="#976856")
    cnt['others'] += 1
    graph.add_node(node)
    s.push(node)
    p[0]={
        'operator':p[1],
        'lineNo':p.lineno(1)
    }
def p_assignment_operator_5(p):
    '''assignment_operator : ADD_ASSIGN'''
    node = pydot.Node("others" + "_%d" % cnt['others'],label=str(p[1]), style="filled", fillcolor="#976856")
    cnt['others'] += 1
    graph.add_node(node)
    s.push(node)
    p[0]={
        'operator':p[1],
        'lineNo':p.lineno(1)
    }
def p_assignment_operator_6(p):
    '''assignment_operator : SUB_ASSIGN'''
    node = pydot.Node("others" + "_%d" % cnt['others'],label=str(p[1]), style="filled", fillcolor="#976856")
    cnt['others'] += 1
    graph.add_node(node)
    s.push(node)
    p[0]={
        'operator':p[1],
        'lineNo':p.lineno(1)
    }
def p_assignment_operator_7(p):
    '''assignment_operator : LEFT_ASSIGN'''
    node = pydot.Node("others" + "_%d" % cnt['others'],label=str(p[1]), style="filled", fillcolor="#976856")
    cnt['others'] += 1
    graph.add_node(node)
    s.push(node)
    p[0]={
        'operator':p[1],
        'lineNo':p.lineno(1)
    }
def p_assignment_operator_8(p):
    '''assignment_operator : RIGHT_ASSIGN'''
    node = pydot.Node("others" + "_%d" % cnt['others'],label=str(p[1]), style="filled", fillcolor="#976856")
    cnt['others'] += 1
    graph.add_node(node)
    s.push(node)
    p[0]={
        'operator':p[1],
        'lineNo':p.lineno(1)
    }
def p_assignment_operator_9(p):
    '''assignment_operator : AND_ASSIGN'''
    node = pydot.Node("others" + "_%d" % cnt['others'],label=str(p[1]), style="filled", fillcolor="#976856")
    cnt['others'] += 1
    graph.add_node(node)
    s.push(node)
    p[0]={
        'operator':p[1],
        'lineNo':p.lineno(1)
    }
def p_assignment_operator_10(p):
    '''assignment_operator : OR_ASSIGN'''
    node = pydot.Node("others" + "_%d" % cnt['others'],label=str(p[1]), style="filled", fillcolor="#976856")
    cnt['others'] += 1
    graph.add_node(node)
    s.push(node)
    p[0]={
        'operator':p[1],
        'lineNo':p.lineno(1)
    }
def p_assignment_operator_11(p):
    '''assignment_operator : XOR_ASSIGN'''
    node = pydot.Node("others" + "_%d" % cnt['others'],label=str(p[1]), style="filled", fillcolor="#976856")
    cnt['others'] += 1
    graph.add_node(node)
    s.push(node)
    p[0]={
        'operator':p[1],
        'lineNo':p.lineno(1)
    }
# conditional-expression
cnt['conditional_expression'] = 0

def p_conditional_expression_1(p):
    'conditional_expression : logical_or_expression'
    p[0]=p[1]


def p_conditional_expression_2(p):
    'conditional_expression : logical_or_expression QUEMARK expression COLON conditional_expression '
    node = pydot.Node("others" + "_%d" % cnt['others'],label="QUEMARK", style="filled", fillcolor="#976856")
    cnt['others'] += 1
    graph.add_node(node)
    s.push(node)
    node = pydot.Node("others" + "_%d" % cnt['others'],label="COLON", style="filled", fillcolor="#976856")
    cnt['others'] += 1
    graph.add_node(node)
    s.push(node)
    func("conditional_expression",5)
    p[0]=p[1]
    p[0]['type']=''
    

# constant-expression
cnt['constant_expression'] = 0

def p_constant_expression(p):
    'constant_expression : conditional_expression'
    p[0] = p[1]

# logical-or-expression
cnt['logical_or_expression'] = 0

def p_logical_or_expression_1(p):
    'logical_or_expression : logical_and_expression'
    p[0]=p[1]

def p_logical_or_expression_2(p):
    'logical_or_expression : logical_or_expression OR_OP dummyM logical_and_expression'
    func("||",2)
    p[0]=p[1]
    tempsrc=p[0]["tempvar"]
    p[0]['type']='bool'
    if p[1]['type']!='int' and p[4]['type']!='int' and p[1]['type']!='bool' and p[4]['type']!='bool':
        print "Error: Operator ",p[2]," requires operands of type int or float. Error at line no", p.lineno(2)
        exit()
    TAC.backpatch(p[1]['falselist'], p[3]['quad']+TAC.code[ST.scopeList[-1]['scope']]["startno"])
    p[0]['truelist'] = TAC.merge(p[1]['truelist'],p[4]['truelist'])
    p[0]['falselist'] = p[4]['falselist']
    p[0]["tempvar"]=TAC.newTemp()

    global offset
    ST.addIdentifier(p[0]["tempvar"],p[0]['type'],0,1,False,offset)
    offset = offset + ST.getSize(p[0]['type'])

    TAC.emit(p[2],p[0]["tempvar"],tempsrc,p[4]["tempvar"])

# logical-and-expression
cnt['logical_and_expression'] = 0

def p_logical_and_expression_1(p):
    'logical_and_expression : inclusive_or_expression'
    p[0]=p[1]

def p_logical_and_expression_2(p):
    'logical_and_expression : logical_and_expression AND_OP dummyM inclusive_or_expression'
    func("&&",2)
    p[0]=p[1]
    tempsrc = p[0]['tempvar']
    p[0]['type']='bool'
    if p[1]['type']!='int' and p[4]['type']!='int' and p[1]['type']!='bool' and p[4]['type']!='bool':
        print "Error: Operator ",p[2]," requires operands of type int or float. Error at line no", p.lineno(2)
        exit()
    TAC.backpatch(p[1]['truelist'], p[3]['quad']+TAC.code[ST.scopeList[-1]['scope']]["startno"])
    p[0]['truelist'] = p[4]['truelist']
    p[0]['falselist'] = TAC.merge(p[1]['falselist'],p[4]['falselist'])
    p[0]['tempvar']=TAC.newTemp()
    TAC.emit(p[2],p[0]["tempvar"],tempsrc,p[4]["tempvar"]+TAC.code[ST.scopeList[-1]['scope']]["startno"])

    global offset
    ST.addIdentifier(p[0]["tempvar"],p[0]['type'],0,1,False,offset)
    offset = offset + ST.getSize(p[0]['type'])


# inclusive-or-expression:
cnt['inclusive_or_expression'] = 0

def p_inclusive_or_expression_1(p):
    'inclusive_or_expression : exclusive_or_expression'
    p[0]=p[1]


def p_inclusive_or_expression_2(p):
    'inclusive_or_expression : inclusive_or_expression OR dummyM exclusive_or_expression'
    func("|",2)
    p[0]=p[1]
    tempsrc = p[0]['tempvar']
    p[0]['type']='bool'
    if p[1]['type']!='int' and p[4]['type']!='int' and p[1]['type']!='bool' and p[4]['type']!='bool':
        print "Error: Operator ",p[2]," requires operands of type int or float. Error at line no", p.lineno(2)
        exit()
    TAC.backpatch(p[1]['falselist'], p[3]['quad']+TAC.code[ST.scopeList[-1]['scope']]["startno"])
    p[0]['truelist'] = TAC.merge(p[1]['truelist'],p[4]['truelist'])
    p[0]['falselist'] = p[4]['falselist']
    p[0]['tempvar']=TAC.newTemp()
    TAC.emit(p[2],p[0]["tempvar"],tempsrc,p[4]["tempvar"])

    global offset
    ST.addIdentifier(p[0]["tempvar"],p[0]['type'],0,1,False,offset)
    offset = offset + ST.getSize(p[0]['type'])


# exclusive-or-expression:
cnt['exclusive_or_expression'] = 0

def p_exclusive_or_expression_1(p):
    'exclusive_or_expression :  and_expression'
    p[0]=p[1]


def p_exclusive_or_expression_2(p):
    'exclusive_or_expression :  exclusive_or_expression XOR dummyM and_expression'
    func("^",2)
    p[0]=p[1]
    tempsrc = p[0]['tempvar']
    print "Operations on",p[2],"ar not defined in our language so considering it as or" 
    p[0]['type']='bool'
    if p[1]['type']!='int' and p[4]['type']!='int' and p[1]['type']!='bool' and p[4]['type']!='bool':
        print "Error: Operator ",p[2]," requires operands of type int or float. Error at line no", p.lineno(2)
        exit()
    TAC.backpatch(p[1]['falselist'], p[3]['quad']+TAC.code[ST.scopeList[-1]['scope']]["startno"])
    p[0]['truelist'] = TAC.merge(p[1]['truelist'],p[4]['truelist'])
    p[0]['falselist'] = p[4]['falselist']
    p[0]['tempvar']=TAC.newTemp()
    TAC.emit("||",p[0]["tempvar"],tempsrc,p[4]["tempvar"])

    global offset
    ST.addIdentifier(p[0]["tempvar"],p[0]['type'],0,1,False,offset)
    offset = offset + ST.getSize(p[0]['type'])


# AND-expression
cnt['and_expression'] = 0

def p_and_expression_1(p):
    'and_expression : equality_expression'
    p[0]=p[1]


def p_and_expression_2(p):
    'and_expression : and_expression AND dummyM equality_expression'
    func("&",2)
    p[0]=p[1]
    tempsrc = p[0]['tempvar']
    p[0]['type']='bool'
    if p[1]['type']!='int' and p[4]['type']!='int' and p[1]['type']!='bool' and p[4]['type']!='bool':
        print "Error: Operator ",p[2]," requires operands of type int or float. Error at line no", p.lineno(2)
        exit()
    TAC.backpatch(p[1]['truelist'], p[3]['quad']+TAC.code[ST.scopeList[-1]['scope']]["startno"])
    p[0]['truelist'] = p[4]['truelist']
    p[0]['falselist'] = TAC.merge(p[1]['falselist'],p[4]['falselist'])
    p[0]['tempvar']=TAC.newTemp()
    TAC.emit(p[2],p[0]["tempvar"],tempsrc,p[4]["tempvar"])

    global offset
    ST.addIdentifier(p[0]["tempvar"],p[0]['type'],0,1,False,offset)
    offset = offset + ST.getSize(p[0]['type'])


# equality-expression:
cnt['equality_expression'] = 0
def p_equality_expression_1(p):
    'equality_expression : relational_expression'
    p[0]=p[1]

def p_equality_expression_2(p):
    'equality_expression : equality_expression EQ_OP relational_expression'
    func("==",2)
    p[0]=p[1]
    tempsrc = p[0]['tempvar']
    
    if p[1]['type']!= p[3]['type']:
        print "Error: Operator ",p[2]," requires operands of same type. Error at line no", p.lineno(2)
        print p[1]['type']+p[3]['type']
        exit()
    
    p[0]["tempvar"]=TAC.newTemp()
    p[0]["truelist"] = p[3]["truelist"]
    p[0]["falselist"] = p[3]["falselist"]

    p[0]["truelist"].append(len(TAC.code[ST.scopeList[-1]['scope']]["tac"]))
    TAC.emit2(p[2],p[0]["tempvar"],tempsrc,p[3]["tempvar"],"")

    p[0]["falselist"].append(len(TAC.code[ST.scopeList[-1]['scope']]["tac"]))
    TAC.emit2("goto","","","","")
    p[0]['type']='bool'


    global offset
    ST.addIdentifier(p[0]["tempvar"],p[0]['type'],0,1,False,offset)
    offset = offset + ST.getSize(p[0]['type'])


def p_equality_expression_3(p):
    'equality_expression : equality_expression NE_OP relational_expression'
    func("!=",2)
    p[0]=p[1]
    tempsrc = p[0]['tempvar']
    
    p[0]['idList']=p[1]['idList']+p[3]['idList']
    if p[1]['type']!= p[3]['type']:
        print "Error: Operator ",p[2]," requires operands of same type. Error at line no", p.lineno(2)
        exit()
    
    p[0]["tempvar"]=TAC.newTemp()
    p[0]["truelist"] = p[3]["truelist"]
    p[0]["falselist"] = p[3]["falselist"]

    p[0]["truelist"].append(len(TAC.code[ST.scopeList[-1]['scope']]["tac"]))
    TAC.emit2(p[2],p[0]["tempvar"],tempsrc,p[3]["tempvar"],"")

    p[0]["falselist"].append(len(TAC.code[ST.scopeList[-1]['scope']]["tac"]))
    TAC.emit2("goto","","","","")
    p[0]['type']='bool'

    global offset
    ST.addIdentifier(p[0]["tempvar"],p[0]['type'],0,1,False,offset)
    offset = offset + ST.getSize(p[0]['type'])


# relational-expression:
cnt['relational_expression'] = 0
def p_relational_expression_1(p):
    'relational_expression : shift_expression'
    p[0]=p[1]
    p[0]["truelist"] = []
    p[0]["falselist"] = []
    

def p_relational_expression_2(p):
    'relational_expression : relational_expression LT shift_expression'
    func("<",2)
    p[0]=p[1]
    tempsrc = p[0]['tempvar']
    
    p[0]['type']='bool'
    if p[1]['type']=='string' or p[3]['type']=='string' or p[1]['type']=='identifier' or p[3]['type']=='identifier':
        print "Error: Operator ",p[2]," requires operands of type int or float. Error at line no", p.lineno(2)
        exit()
    
    p[0]["tempvar"]=TAC.newTemp()
    p[0]["truelist"] = p[1]["truelist"]
    p[0]["falselist"] = p[1]["falselist"]

    p[0]["truelist"].append(len(TAC.code[ST.scopeList[-1]['scope']]["tac"]))
    TAC.emit2(p[2],p[0]["tempvar"],tempsrc,p[3]["tempvar"],"")

    p[0]["falselist"].append(len(TAC.code[ST.scopeList[-1]['scope']]["tac"]))
    TAC.emit2("goto","","","","")
    p[0]['type']='bool'


    global offset
    ST.addIdentifier(p[0]["tempvar"],p[0]['type'],0,1,False,offset)
    offset = offset + ST.getSize(p[0]['type'])



def p_relational_expression_3(p):
    'relational_expression : relational_expression GT shift_expression'
    func(">",2)
    p[0]=p[1]
    tempsrc = p[0]['tempvar']
    
    p[0]['type']='bool'
    if p[1]['type']=='string' or p[3]['type']=='string' or p[1]['type']=='identifier' or p[3]['type']=='identifier':
        print "Error: Operator ",p[2]," requires operands of type int or float. Error at line no", p.lineno(2)
        exit()
    
    p[0]["tempvar"]=TAC.newTemp()
    p[0]["truelist"] = p[1]["truelist"]
    p[0]["falselist"] = p[1]["falselist"]

    p[0]["truelist"].append(len(TAC.code[ST.scopeList[-1]['scope']]["tac"]))
    TAC.emit2(p[2],p[0]["tempvar"],tempsrc,p[3]["tempvar"],"")

    p[0]["falselist"].append(len(TAC.code[ST.scopeList[-1]['scope']]["tac"]))
    TAC.emit2("goto","","","","")
    p[0]['type']='bool'


    global offset
    ST.addIdentifier(p[0]["tempvar"],p[0]['type'],0,1,False,offset)
    offset = offset + ST.getSize(p[0]['type'])


def p_relational_expression_4(p):
    'relational_expression : relational_expression LE_OP shift_expression'
    func("<=",2)
    p[0]=p[1]
    tempsrc = p[0]['tempvar']
    
    p[0]['type']='bool'
    if p[1]['type']=='string' or p[3]['type']=='string' or p[1]['type']=='identifier' or p[3]['type']=='identifier':
        print "Error: Operator ",p[2]," requires operands of type int or float. Error at line no", p.lineno(2)
        exit()
    
    p[0]["tempvar"]=TAC.newTemp()
    p[0]["truelist"] = p[1]["truelist"]
    p[0]["falselist"] = p[1]["falselist"]

    p[0]["truelist"].append(len(TAC.code[ST.scopeList[-1]['scope']]["tac"]))
    TAC.emit2(p[2],p[0]["tempvar"],tempsrc,p[3]["tempvar"],"")

    p[0]["falselist"].append(len(TAC.code[ST.scopeList[-1]['scope']]["tac"]))
    TAC.emit2("goto","","","","")
    p[0]['type']='bool'


    global offset
    ST.addIdentifier(p[0]["tempvar"],p[0]['type'],0,1,False,offset)
    offset = offset + ST.getSize(p[0]['type'])



def p_relational_expression_5(p):
    'relational_expression : relational_expression GE_OP shift_expression'
    func(">=",2)
    p[0]=p[1]
    tempsrc = p[0]['tempvar']
    
    if p[1]['type']=='string' or p[3]['type']=='string' or p[1]['type']=='identifier' or p[3]['type']=='identifier':
        print "Error: Operator ",p[2]," requires operands of type int or float. Error at line no", p.lineno(2)
        exit()
    
    p[0]["tempvar"]=TAC.newTemp()
    p[0]["truelist"] = p[1]["truelist"]
    p[0]["falselist"] = p[1]["falselist"]

    p[0]["truelist"].append(len(TAC.code[ST.scopeList[-1]['scope']]["tac"]))
    TAC.emit2(p[2],p[0]["tempvar"],tempsrc,p[3]["tempvar"],"")

    p[0]["falselist"].append(len(TAC.code[ST.scopeList[-1]['scope']]["tac"]))
    TAC.emit2("goto","","","","")
    p[0]['type']='bool'

    global offset
    ST.addIdentifier(p[0]["tempvar"],p[0]['type'],0,1,False,offset)
    offset = offset + ST.getSize(p[0]['type'])



# shift-expression
cnt['shift_expression'] = 0

def p_shift_expression_1(p):
    'shift_expression : additive_expression'
    p[0]=p[1]

def p_shift_expression_2(p):
    'shift_expression : shift_expression LEFT_OP additive_expression'
    func("<<",2)
    p[0]=p[1]
    tempsrc = p[0]['tempvar']
    
    if p[1]['type']!='int' or p[3]['type']!='int':
        print "Error: Operator ",p[2]," requires operands of type int. Error at line no", p.lineno(2)
        exit()
    p[0]["tempvar"]=TAC.newTemp()
    TAC.emit(p[2],p[0]["tempvar"],tempsrc,p[3]["tempvar"])

    global offset
    ST.addIdentifier(p[0]["tempvar"],p[0]['type'],0,1,False,offset)
    offset = offset + ST.getSize(p[0]['type'])



def p_shift_expression_3(p):
    'shift_expression : shift_expression RIGHT_OP additive_expression'
    func(">>",2)
    p[0]=p[1]
    tempsrc = p[0]['tempvar']
    
    if p[1]['type']!='int' or p[3]['type']!='int':
        print "Error: Operator ",p[2]," requires operands of type int. Error at line no", p.lineno(2)
        exit()

    p[0]["tempvar"]=TAC.newTemp()
    TAC.emit(p[2],p[0]["tempvar"],tempsrc,p[3]["tempvar"])

    global offset
    ST.addIdentifier(p[0]["tempvar"],p[0]['type'],0,1,False,offset)
    offset = offset + ST.getSize(p[0]['type'])


    
# additive-expression
cnt['additive_expression'] = 0

def p_additive_expression_1(p):
    'additive_expression : multiplicative_expression'
    p[0]=p[1]


def p_additive_expression_2(p):
    'additive_expression : additive_expression PLUS multiplicative_expression'
    func("+",2)
    p[0]=p[1]
    tempsrc = p[0]['tempvar']
    
    p[0]["tempvar"]=TAC.newTemp()
    #print p[0]["tempvar"]+"="+p[1]['tempvar']+p[2]+p[3]['tempvar']
   
    if p[1]['type']=='string' or p[3]['type']=='string':
        print "Error: Incompatible operator ",p[2]," with operand of type string at line no", p.lineno(2)
        exit()

    elif p[1]['type']=='int' and p[3]['type']=='int':
        p[0]['type']='int'
        TAC.emit("int"+p[2],p[0]["tempvar"],tempsrc,p[3]["tempvar"])

    else :
        p[0]['type']='float'
        TAC.emit("float"+p[2],p[0]["tempvar"],tempsrc,p[3]["tempvar"])

    global offset
    ST.addIdentifier(p[0]["tempvar"],p[0]['type'],0,1,False,offset)
    offset = offset + ST.getSize(p[0]['type'])


    
def p_additive_expression_3(p):
    'additive_expression : additive_expression MINUS multiplicative_expression'
    func("-",2)
    p[0]=p[1]
    tempsrc = p[0]['tempvar']
    
    if p[1]['type']=='string' or p[3]['type']=='string':
        print "Error: Incompatible operator ",p[2]," with operand of type string at line no", p.lineno(2)
        exit()
    elif p[1]['type']=='int' and p[3]['type']=='int':
        p[0]['type']='int'
        p[0]["tempvar"]=TAC.newTemp()
        TAC.emit("int"+p[2],p[0]["tempvar"],tempsrc,p[3]["tempvar"])
    else :
        p[0]['type']='float'
        p[0]["tempvar"]=TAC.newTemp()
        TAC.emit("float"+p[2],p[0]["tempvar"],tempsrc,p[3]["tempvar"])

    global offset
    ST.addIdentifier(p[0]["tempvar"],p[0]['type'],0,1,False,offset)
    offset = offset + ST.getSize(p[0]['type'])



# multiplicative-expression
cnt['multiplicative_expression'] = 0

def p_multiplicative_expression_1(p):
    'multiplicative_expression : cast_expression'
    p[0]=p[1]


def p_multiplicative_expression_2(p):
    'multiplicative_expression : multiplicative_expression TIMES cast_expression'
    func("*",2)
    p[0]=p[1]
    tempsrc = p[0]['tempvar']
    
    if p[1]['type']=='string' or p[3]['type']=='string':
        print "Error: Incompatible operator * with operand of type string at line no", p.lineno(2)
        exit()
    elif p[1]['type']=='int' and p[3]['type']=='int':
        p[0]['type']='int'
        p[0]["tempvar"]=TAC.newTemp()
        TAC.emit("int"+p[2],p[0]["tempvar"],tempsrc,p[3]["tempvar"])

    else :
        p[0]['type']='float'
        p[0]["tempvar"]=TAC.newTemp()
        TAC.emit("float"+p[2],p[0]["tempvar"],tempsrc,p[3]["tempvar"])

    global offset
    ST.addIdentifier(p[0]["tempvar"],p[0]['type'],0,1,False,offset)
    offset = offset + ST.getSize(p[0]['type'])


def p_multiplicative_expression_3(p):
    'multiplicative_expression : multiplicative_expression DIVIDE cast_expression'
    func("/",2)
    p[0]=p[1]
    tempsrc = p[0]['tempvar']
    
    if p[1]['type']=='string' or p[3]['type']=='string':
        print "Error: Incompatible operator / with operand of type string at line no", p.lineno(2)
        exit()
    elif p[1]['type']=='int' and p[3]['type']=='int':
        p[0]['type']='int'
        p[0]["tempvar"]=TAC.newTemp()
        TAC.emit("int"+p[2],p[0]["tempvar"],tempsrc,p[3]["tempvar"])

    else :
        p[0]['type']='float'
        p[0]["tempvar"]=TAC.newTemp()
        TAC.emit("float"+p[2],p[0]["tempvar"],tempsrc,p[3]["tempvar"])

    global offset
    ST.addIdentifier(p[0]["tempvar"],p[0]['type'],0,1,False,offset)
    offset = offset + ST.getSize(p[0]['type'])


def p_multiplicative_expression_4(p):
    'multiplicative_expression : multiplicative_expression MOD cast_expression'
    func("%",2)
    p[0]=p[1]
    tempsrc = p[0]['tempvar']
    
    if p[1]['type']=='string' or p[3]['type']=='string':
        print "Error: Incompatible operator % with operand of type string at line no", p.lineno(2)
        exit()
    elif p[1]['type']=='int' and p[3]['type']=='int':
        p[0]['type']='int'
        p[0]["tempvar"]=TAC.newTemp()
        TAC.emit("int"+p[2],p[0]["tempvar"],tempsrc,p[3]["tempvar"])
    else :
        p[0]['type']='float'
        p[0]["tempvar"]=TAC.newTemp()
        TAC.emit("float"+p[2],p[0]["tempvar"],tempsrc,p[3]["tempvar"])

    global offset
    ST.addIdentifier(p[0]["tempvar"],p[0]['type'],0,1,False,offset)
    offset = offset + ST.getSize(p[0]['type'])


     
# cast-expression:
cnt['cast_expression'] = 0

def p_cast_expression_1(p):
    'cast_expression : unary_expression'
    p[0]=p[1]


def p_cast_expression_2(p):
    'cast_expression : LPAREN type_name RPAREN cast_expression'
    func("cast_expression",2)
    p[0]=p[4]
    p[0]['type']=p[2]['specifier_qualifier_list']['type_specifier']
    tempsrc = p[0]['tempvar']
    
    p[0]["tempvar"]=TAC.newTemp()
    TAC.emit("cast",p[0]["tempvar"],p[2]['specifier_qualifier_list']['type_specifier'],tempsrc)

    global offset
    ST.addIdentifier(p[0]["tempvar"],p[0]['type'],0,1,False,offset)
    offset = offset + ST.getSize(p[0]['type'])


# unary-expression:
cnt['unary_expression'] = 0

def p_unary_expression_1(p):
    'unary_expression : postfix_expression'
    p[0]=p[1]
    global cnttmpscp
    if cnttmpscp>0:
        for x in xrange(0,cnttmpscp):
           # ST.delScope(-1)
            cnttmpscp = 0

    
def p_unary_expression_2(p):
    'unary_expression : INC_OP unary_expression'
    node = pydot.Node("others" + "_%d" % cnt['others'],label="INC_OP", style="filled", fillcolor="#976856")
    cnt['others'] += 1
    graph.add_node(node)
    s.push(node)
    func("unary_expression",2)
    p[0]=p[2]
    p[0]['type']=''
    tempsrc = p[2]['tempvar']
    
    p[0]["tempvar"]=TAC.newTemp()
    TAC.emit(p[1],p[0]["tempvar"],tempsrc,"")


    global offset
    ST.addIdentifier(p[0]["tempvar"],p[0]['type'],0,1,False,offset)
    offset = offset + ST.getSize(p[0]['type'])


def p_unary_expression_3(p):
    'unary_expression : DEC_OP unary_expression'
    node = pydot.Node("others" + "_%d" % cnt['others'],label="DEC_OP", style="filled", fillcolor="#976856")
    cnt['others'] += 1
    graph.add_node(node)
    s.push(node)
    func("unary_expression",2)
    p[0]=p[2]
    p[0]['type']=''
    tempsrc = p[2]['tempvar']
    

    p[0]["tempvar"]=TAC.newTemp()
    TAC.emit(p[1],p[0]["tempvar"],tempsrc,"")


    global offset
    ST.addIdentifier(p[0]["tempvar"],p[0]['type'],0,1,False,offset)
    offset = offset + ST.getSize(p[0]['type'])


def p_unary_expression_4(p):
    'unary_expression : unary_operator cast_expression'
    func("unary_expression",2)
    p[0]=p[2]
    p[0]['type']=''
    tempsrc = p[2]['tempvar']
    
    p[0]["tempvar"]=TAC.newTemp()
    TAC.emit(p[1],p[0]["tempvar"],tempsrc,"")


    global offset
    ST.addIdentifier(p[0]["tempvar"],p[0]['type'],0,1,False,offset)
    offset = offset + ST.getSize(p[0]['type'])


def p_unary_expression_5(p):
    'unary_expression : SIZEOF unary_expression'
    node = pydot.Node("others" + "_%d" % cnt['others'],label="SIZEOF", style="filled", fillcolor="#976856")
    cnt['others'] += 1
    graph.add_node(node)
    s.push(node)
    func("unary_expression",2)
    p[0]=p[2]
    p[0]['type']='int'
    tempsrc = p[2]['tempvar']
    
    p[0]["tempvar"]=TAC.newTemp()
    TAC.emit(p[1],p[0]["tempvar"],tempsrc,"")

    global offset
    ST.addIdentifier(p[0]["tempvar"],p[0]['type'],0,1,False,offset)
    offset = offset + ST.getSize(p[0]['type'])




def p_unary_expression_6(p):
    'unary_expression : SIZEOF LPAREN type_name RPAREN'
    node = pydot.Node("others" + "_%d" % cnt['others'],label="SIZEOF", style="filled", fillcolor="#976856")
    cnt['others'] += 1
    graph.add_node(node)
    s.push(node)
    func("unary_expression",2)
    p[0]={
        'type':'int'
    }
    p[0]["tempvar"]=TAC.newTemp()

    varSize = 0
    if p[3]['specifier_qualifier_list']['type_specifier'] in ['char']:
        varSize = 1
    elif p[3]['specifier_qualifier_list']['type_specifier'] in ['float','int']:
        varSize = 4
    elif p[3]['specifier_qualifier_list']['type_specifier'] in [ 'double']:
        varSize = 8
    elif p[3]['specifier_qualifier_list']['type_specifier'] in ['FUNCTION','String']:
        varSize = 4                 #address size
    else:
        varSize = -1    
    TAC.emit('=',p[0]["tempvar"],varSize,"")


    global offset
    ST.addIdentifier(p[0]["tempvar"],p[0]['type'],0,1,False,offset)
    offset = offset + ST.getSize(p[0]['type'])


# unary-operator
cnt['unary_operator'] = 0

def p_unary_operator_1(p):
    '''unary_operator : AND'''
    node = pydot.Node("others" + "_%d" % cnt['others'],label="AND", style="filled", fillcolor="#976856")
    cnt['others'] += 1
    graph.add_node(node)
    s.push(node)
    p[0]=p[1]

def p_unary_operator_2(p):
    '''unary_operator : TIMES'''
    node = pydot.Node("others" + "_%d" % cnt['others'],label="TIMES", style="filled", fillcolor="#976856")
    cnt['others'] += 1
    graph.add_node(node)
    s.push(node)
    p[0]=p[1]

def p_unary_operator_3(p):
    '''unary_operator : PLUS'''
    node = pydot.Node("others" + "_%d" % cnt['others'],label="PLUS", style="filled", fillcolor="#976856")
    cnt['others'] += 1
    graph.add_node(node)
    s.push(node)
    p[0]=p[1]

def p_unary_operator_4(p):
    '''unary_operator : MINUS'''
    node = pydot.Node("others" + "_%d" % cnt['others'],label="MINUS", style="filled", fillcolor="#976856")
    cnt['others'] += 1
    graph.add_node(node)
    s.push(node)
    p[0]=p[1]

def p_unary_operator_5(p):
    '''unary_operator : NOT'''
    node = pydot.Node("others" + "_%d" % cnt['others'],label="NOT", style="filled", fillcolor="#976856")
    cnt['others'] += 1
    graph.add_node(node)
    s.push(node)
    p[0]=p[1]

def p_unary_operator_6(p):
    '''unary_operator : LNOT'''
    node = pydot.Node("others" + "_%d" % cnt['others'],label="LNOT", style="filled", fillcolor="#976856")
    cnt['others'] += 1
    graph.add_node(node)
    s.push(node)
    p[0]=p[1]

# postfix-expression:
cnt['postfix_expression'] = 0
cnttmpscp = 0
def p_postfix_expression_1(p):
    'postfix_expression : primary_expression'
    p[0]=p[1]
    if(p[0]['orderDef']<p[0]['order']):
        print "Error at line no",p[0]['lineNo'],":access out of degree of array"
        exit()

def p_postfix_expression_2(p):
    'postfix_expression : postfix_expression LBRACKET expression RBRACKET'
    func("postfix_expression",2)
    p[0]=p[1]
    global sizeArray
    sizeArray = sizeArray + int(p[3]["tempvar"])
    p[0]['order']+=1
    tempsrc=p[0]["tempvar"]
    if(p[0]['orderDef']<p[0]['order']):
        print "Error at line no",p[0]['lineNo'],":access out of degree of array"
        exit()
 
    p[0]["tempvar"]=tempsrc+"["+p[3]["tempvar"]+"]"
    
#    p[0]['value']=str(p[1]['value'])+p[2]+str(p[3]['value'])+p[4]


def p_postfix_expression_3(p):
    'postfix_expression : postfix_expression LPAREN argument_expression_list RPAREN'
    func("postfix_expression",2)
    p[0]=p[1]
    tempsrc=p[0]["tempvar"]
    if(p[0]['orderDef']<p[0]['order']):
        print "Error at line no",p[0]['lineNo'],":access out of degree of array"
        exit()
    p[0]["tempvar"]=TAC.newTemp()

    for scpe in ST.scopeListFull:
        if scpe['scope'] == tempsrc:
            p[0]['type'] = scpe['retype']   
            break
    TAC.emitf("refparam",p[0]["tempvar"])
    TAC.emitf(tempsrc,len(p[3])+1)

    global offset
    ST.addIdentifier(p[0]["tempvar"],p[0]['type'],0,1,False,offset)
    offset = offset + 4


def p_postfix_expression_4(p):
    'postfix_expression : postfix_expression LPAREN RPAREN'
    p[0]=p[1]
    tempsrc=p[0]["tempvar"]
    if(p[0]['orderDef']<p[0]['order']):
        print "Error at line no",p[0]['lineNo'],":access out of degree of array"
        exit()
    p[0]["tempvar"]=TAC.newTemp()
    TAC.emitf("refparam",p[0]["tempvar"])
    TAC.emitf(tempsrc,1)
    for scpe in ST.scopeListFull:
        if scpe['scope']== tempsrc:
            p[0]['type'] = scpe['retype']
            break
    global offset
    ST.addIdentifier(p[0]["tempvar"],p[0]['type'],0,1,False,offset)
    offset = offset + 4


def p_postfix_expression_5(p):
    'postfix_expression : postfix_expression PERIOD IDENTIFIER'
    node = pydot.Node("others" + "_%d" % cnt['others'],label="PERIOD", style="filled", fillcolor="#976856")
    cnt['others'] += 1
    graph.add_node(node)
    s.push(node)
    node = pydot.Node("IDENTIFIER" + "_%d" % cnt['IDENTIFIER'],label=p[3], style="filled", fillcolor="#976856")
    cnt['IDENTIFIER']+=1
    graph.add_node(node)
    s.push(node)
    func("postfix_expression",3)
    p[0]=p[1]
    
    temp = ST.lookup("____"+p[1]['value'])
    global cnttmpscp
    #print temp
    #print p[1]['value']
    p[1]['orderDef']=temp['order']
    if(p[1]['orderDef']<p[1]['order']):
        print "Error at line no",p[1]['lineNo'],":access out of degree of array"
        exit()

    if temp == None :
        print "Variable \'",p[1]['value'],"\' at line no ",p.lineno(3)," not declared in the scope"
        exit()

    for scpe in ST.scopeListFull:
        if scpe['scope']=="____"+temp['structName']:
            structDecl = scpe
    ST.scopeList.append(structDecl)

    if not structDecl.has_key("____"+p[3]):
        if not structDecl.has_key(p[3]):
            print "Error: Struct ",structDecl['scope'],"has no variable named",p[3]
            print structDecl
            exit()
        else :
            p[0]['orderDef']=structDecl[p[3]]['order']
    else :
        p[0]['orderDef']=structDecl["____"+p[3]]['order']
    

    p[0]['value']=p[3]
    p[0]['order']=0
    p[0]['type']=''


def p_postfix_expression_6(p):
    'postfix_expression : postfix_expression PTR_OP IDENTIFIER'
    global cnttmpscp
    node = pydot.Node("others" + "_%d" % cnt['others'],label="PTR_OP", style="filled", fillcolor="#976856")
    graph.add_node(node)
    cnt['others'] += 1
    s.push(node)
    node = pydot.Node("IDENTIFIER" + "_%d" % cnt['IDENTIFIER'],label=p[3], style="filled", fillcolor="#976856")
    graph.add_node(node)
    cnt['IDENTIFIER']+=1
    s.push(node)
    func("postfix_expression",3)
    p[0]=p[1]
    
    temp = ST.lookup("____"+p[1]['value'])
    global cnttmpscp
    #print temp
    #print p[1]['value']
    p[1]['orderDef']=temp['order']
    if(p[1]['orderDef']<p[1]['order']):
        print "Error at line no",p[1]['lineNo'],":access out of degree of array"
        exit()

    if temp == None :
        print "Variable \'",p[1]['value'],"\' at line no ",p.lineno(3)," not declared in the scope"
        exit()

    for scpe in ST.scopeListFull:
        if scpe['scope']=="____"+temp['structName']:
            structDecl = scpe
    ST.scopeList.append(structDecl)


    if not structDecl.has_key("____"+p[3]):
        if not structDecl.has_key(p[3]):
            print "Error: Struct ",structDecl['scope'],"has no variable named",p[3]
            print structDecl
            exit()
        else :
            p[0]['orderDef']=structDecl[p[3]]['order']
    else :
        p[0]['orderDef']=structDecl["____"+p[3]]['order']
    

    p[0]['value']=p[3]
    p[0]['order']=0
    p[0]['type']=''

def p_postfix_expression_7(p):
    'postfix_expression : postfix_expression INC_OP'
    node = pydot.Node("others" + "_%d" % cnt['others'],label="INC_OP", style="filled", fillcolor="#976856")
    graph.add_node(node)
    cnt['others'] += 1
    s.push(node)
    func("postfix_expression",2)
    p[0]=p[1]
    tempsrc = p[1]['tempvar']
    if(p[0]['orderDef']<p[0]['order']):
        print "Error at line no",p[0]['lineNo'],":access out of degree of array"
        exit()

    TAC.emit(tempsrc,p[0]["tempvar"],p[2],"")

def p_postfix_expression_8(p):
    'postfix_expression : postfix_expression DEC_OP'
    node = pydot.Node("others" + "_%d" % cnt['others'],label="DEC_OP", style="filled", fillcolor="#976856")
    graph.add_node(node)
    cnt['others'] += 1
    s.push(node)
    func("postfix_expression",2)
    p[0]=p[1]
    tempsrc = p[1]['tempvar']
    if(p[0]['orderDef']<p[0]['order']):
        print "Error at line no",p[0]['lineNo'],":access out of degree of array"
        exit()

    p[0]["tempvar"]=TAC.newTemp()
    TAC.emit(tempsrc,p[0]["tempvar"],p[2],"")

    global offset
    ST.addIdentifier(p[0]["tempvar"],p[0]['type'],0,1,False,offset)
    offset = offset + ST.getSize(p[0]['type'])


# primary-expression:
cnt['primary_expression'] = 0

def p_primary_expression_1(p):
    '''primary_expression :  IDENTIFIER'''
    node = pydot.Node("IDENTIFIER" + "_%d" % cnt['IDENTIFIER'],label=p[1], style="filled", fillcolor="#976856")
    cnt['IDENTIFIER']+=1
    graph.add_node(node)
    s.push(node)
    temp = ST.lookup(p[1])
    temp2 = ST.lookup("____"+p[1])
    if temp == None and temp2 == None :
        print "Variable \'",p[1],"\' at line no ",p.lineno(1)," not declared in the scope"
        exit()
    if temp != None: 
        p[0]={
            "value":p[1],
            "type":temp['type'],
            "lineNo":p.lineno(1),
            "orderDef": int(temp['order']),
            "order":0,
            "tempvar":p[1]

        }
    if temp2 != None: 
        p[0]={
            "value":p[1],
            "type":temp2['type'],
            "lineNo":p.lineno(1),
            "orderDef": int(temp2['order']),
            "order":0,
            "tempvar":p[1]

        }
def p_primary_expression_2(p):
    '''primary_expression :  INTEGERNO'''
    node = pydot.Node("others" + "_%d" % cnt['others'],label=p[1], style="filled", fillcolor="#976856")
    cnt['others'] += 1
    graph.add_node(node)
    s.push(node)
    p[0]={
    "value":p[1],
    "type":"int",
    "lineNo":p.lineno(1),
    "orderDef": 0,
    "order":0,
    "tempvar":p[1]

    }
def p_primary_expression_3(p):
    '''primary_expression :  FLOATNO'''
    node = pydot.Node("others" + "_%d" % cnt['others'],label=p[1], style="filled", fillcolor="#976856")
    cnt['others'] += 1
    graph.add_node(node)
    s.push(node)
    p[0]={
        "value":p[1],
        "type":"float",
        "lineNo":p.lineno(1),
        "orderDef": 0,
        "order":0,
        "tempvar":p[1]
    }

def p_primary_expression_4(p):
    '''primary_expression :  STRING_LITERAL'''
    node = pydot.Node("others" + "_%d" % cnt['others'],label="STRING_LITERAL", style="filled", fillcolor="#976856")
    cnt['others'] += 1
    graph.add_node(node)
    s.push(node)
    p[0]={
        "value":p[1],
        "type":"string",
        "lineNo":p.lineno(1),
        "orderDef": 0,
        "order":0,
        "tempvar":p[1]
    }

def p_primary_expression_5(p):
    '''primary_expression :  LPAREN expression RPAREN'''
    p[0]=p[2]
    p[0]['lineNo']=p.lineno(1)
    p[0]={
        "value":'',
        "type":p[2]['type'],
        "lineNo":p.lineno(1),
        "orderDef": 0,
        "order":0,
        "tempvar":p[2]['tempvar']
    }
#    p[0]={
#    "value":p[1]+p[2]+p[3],
#    "type":"expression"
#    }

# argument-expression-list:
cnt['argument_expression_list'] = 0

def p_argument_expression_list_1(p):
    '''argument_expression_list :  assignment_expression'''
    p[0]=[p[1]]
    TAC.emitf("refparam",p[1]["tempvar"])
def p_argument_expression_list_2(p):
    '''argument_expression_list : argument_expression_list COMMA assignment_expression'''
    func("argument_expression_list",2)
    p[0]=p[1]
    p[0].append(p[3])
    TAC.emitf("refparam",p[3]["tempvar"])

cnt['empty'] = 0
def p_dummyM(p):
    'dummyM : empty2'
    p[0]={
        'quad' : len(TAC.code[ST.scopeList[-1]['scope']]["tac"])
    }
def p_dummyN(p):
    'dummyN : empty2'
    p[0]={
        'nextList' : [len(TAC.code[ST.scopeList[-1]['scope']]["tac"])],
    }
    TAC.emit2("goto","","","","")
    
def p_empty2(p):
    'empty2 :  '

def p_empty(p):
    'empty :  '
    node = pydot.Node("others" + "_%d" % cnt['others'],label="EPSILON", style="filled", fillcolor="#976856")
    cnt['others'] += 1
    graph.add_node(node)
    s.push(node)
def p_error(p):
    if p:
         print "Syntax error at line number", p.lineno
         #exit()
         # Just discard the token and tell the parser it's okay.
         parser.errok()
    else:
         print("Syntax error at EOF")
#import profile
# Build the grammar

#cpars = yacc.yacc()
#data = open(sys.argv[1]).read()
#prog = cpars.parse(data)
#if not prog:
#    raise SystemExit

# Build the parser
#parser = yacc.yacc()
#
#while True:
#   try:
#       s = raw_input('lang > ')
#   except EOFError:
#       break
#   if not s: continue
#   result = parser.parse(s)
#   print(result)
parser = yacc.yacc(errorlog=yacc.NullLogger())

with open(sys.argv[1], 'r') as my_file:
    dataInput = my_file.read()

parser.parse(dataInput)

graph.write_png('ast.png')
graph.write_dot('parse_tree.dot')
print("Abstract Syntax Tee generated in ast.png")
pp = pprint.PrettyPrinter(indent=4)
print "\nSymbol Table :"
#pp.pprint(ST.table)
#pp.pprint(ST.scopeList)
pp.pprint(ST.scopeListFull)

pp.pprint(TAC.code)

CG.parseTac()
CG.printMipsCode()
TAC.printCode()