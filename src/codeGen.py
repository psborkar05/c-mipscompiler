import symtab
import tac
import pprint
import re
from pythonds.basic.stack import Stack
s=Stack()
s2=Stack()

pp = pprint.PrettyPrinter(indent=4)

class CodeGen:
	def __init__(self,ST,TAC):
		self.text = []
		self.data = []
		self.bss = []
		self.ST = ST
		self.TAC =TAC
		self.regToVar = {
			'$t0':[],'$t1':[],'$t2':[],'$t3':[],
			'$t4':[],'$t5':[],'$t6':[],'$t7':[],
			'$t8':[],'$t9':[],'$s0':[],'$s1':[],
			'$s2':[],'$s3':[],'$s4':[],'$s5':[],
			'$s6':[],'$s7':[]
		}
		self.regfToVar = {
			'$f0':[],'$f1':[],'$f2':[],'$f3':[],
			'$f4':[],'$f5':[],'$f6':[],'$f7':[],
			'$f8':[],'$f9':[],'$f10':[],'$f11':[],
			'$f12':[],'$f13':[],'$f14':[],'$f15':[],
			'$f16':[],'$f17':[],'$f18':[],'$f19':[],
			'$f20':[],'$f21':[],'$f22':[],'$f23':[],
			'$f24':[],'$f25':[],'$f26':[],'$f27':[],
			'$f28':[],'$f29':[]
		}
		self.varList = {}
	def flushReg(self):
		pass
	def getReg(self,label,scope):
		temp = 10000
		tempreg = '$s0'
		for reg in self.regToVar:
			if len(self.regToVar[reg])==0:
				self.varList[scope][label]['reg']=str(reg)
				self.regToVar[reg].append(label)
				return str(reg)
				break
			else:
				if (temp > len(self.regToVar[reg])):
					temp = len(self.regToVar[reg])
					tempreg = str(reg)
					
		for var in self.varList[scope]:
			self.varList[scope][var]['reg']=''
			self.text.append("\t"+'sw '+tempreg+',-'+str(self.varList[scope][var]['offset'])+'($sp)')
		self.regToVar[tempreg] = []
		return str(tempreg)		

	def getfReg(self,label,scope):
		temp = 1000000
		tempreg = '$s0'
		for reg in self.regfToVar:
			if len(self.regfToVar[reg])==0:
				self.varList[scope][label]['reg']=str(reg)
				self.regfToVar[reg].append(label)
				return str(reg)
			else:
				if (temp > len(self.regfToVar[reg])):
					temp = len(self.regfToVar[reg])
					tempreg = str(reg)
		
		for var in self.varList[scope]:
			self.varList[scope][var]['reg']=''
			self.text.append("\t"+'sw '+tempreg+',-'+str(self.varList[scope][var]['offset'])+'($sp)')
		self.regfToVar[tempreg] = []
		return str(tempreg)		

	def get(self,label,scope):
		if self.varList[scope][label]['reg']!='':
			if not re.match('\$f',self.varList[scope][label]['reg']):
				return self.varList[scope][label]['reg']
			else:
				self.text.append("\t"+'cvt.w.s '+self.varList[scope][label]['reg']+self.varList[scope][label]['reg'])
				self.text.append("\t"+'swc1 '+self.varList[scope][label]['reg']+',-'+str(self.varList[scope][label]['offset'])+'($sp)')
				self.regfToVar[self.varList[scope][label]['reg']] = []
				self.varList[scope][label]['reg'] = ''
				return self.getReg(label,scope)
		return self.getReg(label,scope)

	def getf(self,label,scope):
		if self.varList[scope][label]['reg']!='':
			if re.match('$f'+r'\d',self.varList[scope][label]['reg']):
				return self.varList[scope][label]['reg']
			else:
				self.text.append("\t"+'mtc1 '+self.varList[scope][label]['reg']+',$f30')
				self.text.append("\t"+'cvt.s.w $f30,$f30')
				self.text.append("\t"+'mfc1 '+self.varList[scope][label]['reg']+',$f30')
				self.text.append("\t"+'sw '+self.varList[scope][label]['reg']+',-'+str(self.varList[scope][label]['offset'])+'($sp)')
				self.regToVar[self.varList[scope][label]['reg']] = []
				self.varList[scope][label]['reg'] = ''
				return self.getfReg(label,scope)

		return self.getfReg(label,scope)


	def parseTac(self):
		
		#for the data part
		programScope = self.ST.scopeList[0]
		for key in programScope:
			if isinstance(programScope[key],dict):
				if programScope[key]['type']!= 'function':
					if not programScope[key].has_key('value') or programScope[key]['value']=='':
						self.data.append("\t"+key+":"+"\t"+'.space '+str(programScope[key]['size']))
					else:
						if programScope[key]['type'] == 'int':
							if programScope[key]['order']==0:
								self.data.append("\t"+key+":"+"\t"+'.word '+programScope[key]['value'])
							else:
								print "global array not Supported"
						if programScope[key]['type'] == 'float':
							if programScope[key]['order']==0:
								self.data.append("\t"+key+":"+"\t"+'.float '+programScope[key]['value'])
							else:
								print "global array not Supported"
						if programScope[key]['type'] == 'char':
							if programScope[key]['order']==0:
								self.data.append("\t"+key+":"+"\t"+'.byte '+programScope[key]['value'])
							else:
								self.data.append("\t"+key+":"+"\t"+'.asciiz '+programScope[key]['value'])
		
		self.text.append('main:')

		for scpe in self.ST.scopeListFull:
					if scpe['scope'] == 'main':
						currScope = scpe
						break
		l = {}
		s.push("main")
		while not s.isEmpty():
			tempPop = s.pop()
			for scpe in self.ST.scopeListFull:
					if scpe['scope'] == tempPop:
						curScope = scpe
			for key in curScope:
				if isinstance(curScope[key],dict):
					if curScope[key]['type']!= 'function' and curScope[key]['type']!= '':
						l[key] = {
							'offset':curScope[key]['offset'],
							'reg':''
							}
					elif re.match('scope_'+r'\d',key):
						s.push(str(key))
		
		self.varList[currScope['scope']] = l
		self.text.append("\t"+'addi $a3,$sp,-'+str(currScope['size']))
		self.text.append("\t"+'addi $a2,$zero,-'+str(currScope['size']))
		self.text.append("\t"+'sw $a2,0($a3)')
		self.text.append("\t"+'addi $a3,$a3,-4')
		##<needed?
		self.text.append("\t"+'move $a2,$a3')
		##needed>
		self.text.append("\t"+'addi $a3,$a3,-4')

			
		for idx,quad in enumerate(self.TAC.code["main"]["tac"]):
			#####a3 will be used to point end of currScope
			
			self.text.append("l"+str(idx+self.TAC.code["main"]["startno"])+":")
			if len(quad)==1 and quad[0] == "exit_fun":
				self.text.append("\t"+'li'+" "+'$v0,10')
				self.text.append("\t"+'syscall')
			elif len(quad) == 2:
				if quad[0]=="refparam":
					if currScope[quad[1]]['size'] == 1:
						self.text.append("\t"+'sb '+ self.get(quad[1],currScope['scope']) +',0($a3)')
						self.text.append("\t"+'addi $a3,$a3,-1')
					else:
						if currScope[quad[1]]['type']=='int':
							self.text.append("\t"+'sw '+  self.get(quad[1],currScope['scope']) +',0($a3)')
						else:
							self.text.append("\t"+'swc1 '+  self.getf(quad[1],currScope['scope']) +',0($a3)')
						self.text.append("\t"+'addi $a3,$a3,-4')
					#print "\t"+quad[1]+" size = "+str()
				else:
					for scpe in self.ST.scopeListFull:
						if scpe['scope']==quad[0]:
							paramsize = 0
							for param in scpe['parameterList']:
								paramsize = paramsize + param['size']
							retsize = self.ST.getSize(scpe['retype'])

					self.text.append("\t"+'sw $ra,'+str(4+paramsize+retsize)+'($a2)')
					self.text.append("\t"+'move $sp,$a3')
					self.flushReg()
					self.text.append("\t"+'jal '+str(quad[0]))
						
					#print "\tCALL "+quad[0]+" "+ str(quad[1])

			elif len(quad) == 5:
				if quad[0]=="goto":
					self.text.append("\t"+'j l'+str(quad[-1]))
				elif quad[0]=="return":
					exit()
				else :
					#######
					if self.varList['main'].has_key(quad[2]):
						temp1=self.get(quad[2],"main")
					else:
						temp1=quad[2]
					if self.varList['main'].has_key(quad[3]):
						temp2=self.get(quad[3],"main")
					else:
						temp2=quad[3]
					########
					if quad[0]=='<':
						self.text.append("\t"+'blt '+ temp1 +', '+ temp2 +', l'+quad[-1])
					elif quad[0]=='>':
						self.text.append("\t"+'bgt '+ temp1 +', '+ temp2 +', l'+quad[-1])
					elif quad[0]=='>=':
						self.text.append("\t"+'bge '+ temp1 +', '+ temp2 +', l'+quad[-1])
					elif quad[0]=='<=':
						self.text.append("\t"+'ble '+ temp1 +', '+ temp2 +', l'+quad[-1])
					elif quad[0]=='!=':
						self.text.append("\t"+'bne '+ temp1 +', '+ temp2 +', l'+quad[-1])
					elif quad[0]=='==':
						self.text.append("\t"+'beq '+ temp1 +', '+ temp2 +', l'+quad[-1])
			else:
				if quad[0]=="=" or quad[0]=="+=" or quad[0]=="-=" or quad[0]=="*=" or quad[0]=="/=" or quad[0]=="%=" or quad[0]=="^=" or quad[0]=="&=" or quad[0]=="|=" or quad[0]==">>=" or quad[0]=="<<=" :
					#########
					if self.varList['main'].has_key(quad[1]):
						temp1=self.get(quad[1],"main")
					else:
						temp1=quad[1]
					if self.varList['main'].has_key(quad[2]):
						temp2=self.get(quad[2],"main")
					else:
						temp2=quad[2]

					#########
					if quad[0]=="=":
						self.text.append("\t"+'move '+ temp1 +', '+ temp2)
					elif quad[0]=="+=":
						self.text.append("\t"+'add '+ temp1 +', '+ temp1 +', '+ temp2)
					elif quad[0]=="-=":
						self.text.append("\t"+'sub '+ temp1 +', '+ temp1 +', '+ temp2)
					elif quad[0]=="*=":
						self.text.append("\t"+'mul '+ temp1 +', '+ temp1 +', '+ temp2)
					elif quad[0]=="/=":
						self.text.append("\t"+'div '+ temp1 +', '+ temp1 +', '+ temp2)
					elif quad[0]=="&=":
						self.text.append("\t"+'and '+ temp1 +', '+ temp1 +', '+ temp2)
					elif quad[0]=="|=":
						self.text.append("\t"+'or '+ temp1 +', '+ temp1 +', '+ temp2)
				elif quad[0]=="cast":
					#########
					if self.varList['main'].has_key(quad[1]):
						temp1=self.get(quad[1],"main")
					else:
						temp1=quad[1]
					if self.varList['main'].has_key(quad[3]):
						temp2=self.get(quad[3],"main")
					else:
						temp2=quad[3]
					#########

					if currScope[quad[-1].split("[")[0]]['type']==quad[2]:
						pass
					else:
						if currScope[quad[-1].split("[")[0]]['type'] == 'int':
							self.text.append("\t"+'cvt.s.w '+ temp1 +', '+ temp2)
						elif currScope[quad[-1].split("[")[0]]['type'] == 'float':
							self.text.append("\t"+'cvt.w.s '+ temp1 +', '+ temp2)


			#		print "\t"+quad[1]+" = "+quad[2]+" ("+quad[3]+") "
				elif quad[0]=="[]":
					pass
				#	print "\t"+quad[1]+" = "+quad[2]+" ["+quad[3]+"] "
				elif quad[-1]=="":
					print quad
					#########
					if self.varList['main'].has_key(quad[0]):
						temp1=self.get(quad[0],"main")
					else:
						temp1=quad[0]
					if self.varList['main'].has_key(quad[1]):
						temp2=self.get(quad[1],"main")
					else:
						temp2=quad[1]
					#########
					if quad[2]=='++':
						self.text.append("\t"+'addi '+ temp1 +', '+ temp2 +', '+ str(1))
					elif quad[2]=='--':
						self.text.append("\t"+'addi '+ temp1 +', '+ temp2 +', '+ str(-1))
					elif quad[2]=='-':
						self.text.append("\t"+'mul '+ temp1 +', '+ temp2 +', '+ str(-1))
				
				elif len(quad)!=1:
					#########
					if self.varList['main'].has_key(quad[1]):
						temp1=self.get(quad[1],"main")
					else:
						temp1=quad[1]
					if self.varList['main'].has_key(quad[2]):
						temp2=self.get(quad[2],"main")
					else:
						temp2=quad[2]
					if self.varList['main'].has_key(quad[3]):
						temp3=self.get(quad[3],"main")
					else:
						temp3=quad[3]
					#########
					if re.match('int',quad[0]) or quad[0]== 'float%':
						######
						if self.varList['main'].has_key(quad[1]):
							temp1=self.get(quad[1],"main")
						else:
							temp1=quad[1]
						if self.varList['main'].has_key(quad[2]):
							temp2=self.get(quad[2],"main")
						else:
							temp2=quad[2]
						if self.varList['main'].has_key(quad[3]):
							temp3=self.get(quad[3],"main")
						else:
							temp3=quad[3]
						self.text.append("\t"+'lw '+ temp1 +',-'+ str(currScope[quad[1]]['offset'])+'($sp)')
						self.text.append("\t"+'lw '+ temp2 +',-'+ str(currScope[quad[2]]['offset'])+'($sp)')
						self.text.append("\t"+'lw '+ temp3 +',-'+ str(currScope[quad[3]]['offset'])+'($sp)')
						#######
						if quad[0] == 'int+':
							self.text.append("\t"+'add '+ temp1 +', '+ temp2 +', '+ temp3)
						if quad[0] == 'int-':
							self.text.append("\t"+'sub '+ temp1 +', '+ temp2 +', '+ temp3)
						if quad[0] == 'int*':
							self.text.append("\t"+'mul '+ temp1 +', '+ temp2 +', '+ temp3)
						if quad[0] == 'int/':
							self.text.append("\t"+'div '+ temp1 +', '+ temp2 +', '+ temp3)
						if quad[0] == 'int%' or quad[0] == 'float%':
							self.text.append("\t"+'div '+ temp2 +', '+ temp3)
							self.text.append("\t"+'mfhi '+ temp1)
					else:
						#########

						if self.varList['main'].has_key(quad[1]):
							temp1=self.getf(quad[1],"main")
						else:
							temp1=quad[1]
						if self.varList['main'].has_key(quad[2]):
							temp2=self.getf(quad[2],"main")
						else:
							temp2=quad[2]
						if self.varList['main'].has_key(quad[3]):
							temp3=self.getf(quad[3],"main")
						else:
							temp3=quad[3]
						self.text.append("\t"+'lwc1 '+ temp1 +',-'+ str(currScope[quad[1]]['offset'])+'($sp)')
						self.text.append("\t"+'lwc1 '+ temp2 +',-'+ str(currScope[quad[2]]['offset'])+'($sp)')
						self.text.append("\t"+'lwc1 '+ temp3 +',-'+ str(currScope[quad[3]]['offset'])+'($sp)')
						#print quad
						#print temp1,temp2,temp3
						#exit()
						###########
						if quad[0] == 'float+':
							self.text.append("\t"+'add.s '+ temp1 +', '+ temp2 +', '+ temp3)
						if quad[0] == 'float-':
							self.text.append("\t"+'sub.s '+ temp1 +', '+ temp2 +', '+ temp3)
						if quad[0] == 'float*':
							self.text.append("\t"+'mul.s '+ temp1 +', '+ temp2 +', '+ temp3)
						if quad[0] == 'float/':
							self.text.append("\t"+'div.s '+ temp1 +', '+ temp2 +', '+ temp3)
											
				else:
					pass

		
		print "#\n"
		for i in self.TAC.code:
			if i!= 'main' and i != 'program' and re.match('scope_'+r'\d',i) is None :
				self.text.append(i+':')

				for scpe in self.ST.scopeListFull:
							if scpe['scope'] == i:
								currScope = scpe
								break
				l = {}
				s2.push("main")
				while not s2.isEmpty():
					tempPop = s2.pop()
					for scpe in self.ST.scopeListFull:
							if scpe['scope'] == tempPop:
								curScope = scpe
					for key in curScope:
						if isinstance(curScope[key],dict):
							if curScope[key]['type']!= 'function' and curScope[key]['type']!= '':
								l[key] = {
									'offset':curScope[key]['offset'],
									'reg':''
									}
							elif re.match('scope_'+r'\d',key):
								s2.push(str(key))
				
				self.varList[currScope['scope']] = l
				self.text.append("\t"+'addi $a3,$sp,-'+str(currScope['size']))
				self.text.append("\t"+'addi $a2,$zero,-'+str(currScope['size']))
				self.text.append("\t"+'sw $a2,0($a3)')
				self.text.append("\t"+'addi $a3,$a3,-4')

				self.text.append("\t"+'move $a2,$a3')

				self.text.append("\t"+'addi $a3,$a3,-4')
				

				paramLen = 0
				for param in scpe['parameterList']:
					paramLen = paramLen + param['size']
				offset = 0
				reSize = self.ST.getSize(currScope['retype'])
				for j in scpe['parameterList']:
					self.text.append("\t"+'lw $a0,'+str((paramLen-offset+reSize))+'($sp)')
					self.text.append("\t"+'sw $a0,-'+str(offset)+'($sp)')
					offset = offset + j['size']
				self.text.append("l"+str(self.TAC.code[i]["startno"])+":")
				
				for idx,quad in enumerate(self.TAC.code[i]["tac"]):
					#####a3 will be used to point end of currScope
					
					self.text.append("l"+str(idx+self.TAC.code[i]["startno"])+":")
					if len(quad)==1 and quad[0] == "exit_fun":
						self.text.append("\t"+'lw'+" "+'$ra,'+str(paramLen+reSize+4)+'($sp)')
						self.text.append("\t"+'jr $ra')
					elif len(quad) == 2:
						if quad[0]=="refparam":
							if currScope[quad[1]]['size'] == 1:
								self.text.append("\t"+'sb '+ self.get(quad[1],currScope['scope']) +',0($a3)')
								self.text.append("\t"+'addi $a3,$a3,-1')
							else:
								self.text.append("\t"+'sw '+  self.get(quad[1],currScope['scope']) +',0($a3)')
								self.text.append("\t"+'addi $a3,$a3,-4')
							#print "\t"+quad[1]+" size = "+str()
						else:
							self.text.append("\t"+'sw $ra,'+str(4+paramLen+reSize)+'($sp)')
							self.text.append("\t"+'move $sp,$a3')
							self.flushReg()
							self.text.append("\t"+'jal '+str(quad[0]))
								
							#print "\tCALL "+quad[0]+" "+ str(quad[1])

					elif len(quad) == 5:
						if quad[0]=="goto":
							self.text.append("\t"+'j l'+str(quad[-1]))
						elif quad[0]=="return":
							self.text.append("\t"+'sw'+' '+str(quad[-1])+','+str(reSize)+'($sp)')
							self.text.append("\t"+'lw'+" "+'$ra,'+str(paramLen+reSize+4)+'($sp)')
							self.text.append("\t"+'jr $ra')

						else :
							#######
							if self.varList[i].has_key(quad[2]):
								temp1=self.get(quad[2],i)
							else:
								temp1=quad[2]
							if self.varList[i].has_key(quad[3]):
								temp2=self.get(quad[3],i)
							else:
								temp2=quad[3]
							########
							if quad[0]=='<':
								self.text.append("\t"+'blt '+ temp1 +', '+ temp2 +', l'+quad[-1])
							elif quad[0]=='>':
								self.text.append("\t"+'bgt '+ temp1 +', '+ temp2 +', l'+quad[-1])
							elif quad[0]=='>=':
								self.text.append("\t"+'bge '+ temp1 +', '+ temp2 +', l'+quad[-1])
							elif quad[0]=='<=':
								self.text.append("\t"+'ble '+ temp1 +', '+ temp2 +', l'+quad[-1])
							elif quad[0]=='!=':
								self.text.append("\t"+'bne '+ temp1 +', '+ temp2 +', l'+quad[-1])
							elif quad[0]=='==':
								self.text.append("\t"+'beq '+ temp1 +', '+ temp2 +', l'+quad[-1])
					else:
						if quad[0]=="=" or quad[0]=="+=" or quad[0]=="-=" or quad[0]=="*=" or quad[0]=="/=" or quad[0]=="%=" or quad[0]=="^=" or quad[0]=="&=" or quad[0]=="|=" or quad[0]==">>=" or quad[0]=="<<=" :
							#########
							if self.varList[i].has_key(quad[1]):
								temp1=self.get(quad[1],i)
							else:
								temp1=quad[1]
							if self.varList[i].has_key(quad[2]):
								temp2=self.get(quad[2],i)
							else:
								temp2=quad[2]
							#########
							if quad[0]=="=":
								self.text.append("\t"+'move '+ temp1 +', '+ temp2)
							elif quad[0]=="+=":
								self.text.append("\t"+'add '+ temp1 +', '+ temp1 +', '+ temp2)
							elif quad[0]=="-=":
								self.text.append("\t"+'sub '+ temp1 +', '+ temp1 +', '+ temp2)
							elif quad[0]=="*=":
								self.text.append("\t"+'mul '+ temp1 +', '+ temp1 +', '+ temp2)
							elif quad[0]=="/=":
								self.text.append("\t"+'div '+ temp1 +', '+ temp1 +', '+ temp2)
							elif quad[0]=="&=":
								self.text.append("\t"+'and '+ temp1 +', '+ temp1 +', '+ temp2)
							elif quad[0]=="|=":
								self.text.append("\t"+'or '+ temp1 +', '+ temp1 +', '+ temp2)
						elif quad[0]=="cast":
							pass
					#		print "\t"+quad[1]+" = "+quad[2]+" ("+quad[3]+") "
						elif quad[0]=="[]":
							pass
						#	print "\t"+quad[1]+" = "+quad[2]+" ["+quad[3]+"] "
						elif quad[-1]=="":
							print quad
							#########
							if self.varList[i].has_key(quad[1]):
								temp1=self.get(quad[1],i)
							else:
								temp1=quad[1]
							if self.varList[i].has_key(quad[2]):
								temp2=self.get(quad[2],i)
							else:
								temp2=quad[2]
							#########
							if quad[0]=='++':
								self.text.append("\t"+'addi '+ temp1 +', '+ temp2 +', '+ str(1))
							elif quad[0]=='--':
								self.text.append("\t"+'addi '+ temp1 +', '+ temp2 +', '+ str(-1))
							elif quad[0]=='-':
								self.text.append("\t"+'mul '+ temp1 +', '+ temp2 +', '+ str(-1))
						elif len(quad)!=1:
							#########
							if self.varList[i].has_key(quad[1]):
								temp1=self.get(quad[1],i)
							else:
								temp1=quad[1]
							if self.varList[i].has_key(quad[2]):
								temp2=self.get(quad[2],i)
							else:
								temp2=quad[2]
							if self.varList[i].has_key(quad[3]):
								temp3=self.get(quad[3],i)
							else:
								temp3=quad[3]
							#########
							if quad[0] == 'int+':
								self.text.append("\t"+'add '+ temp1 +', '+ temp2 +', '+ temp3)
							if quad[0] == 'int-':
								self.text.append("\t"+'sub '+ temp1 +', '+ temp2 +', '+ temp3)
							if quad[0] == 'int*':
								self.text.append("\t"+'mul '+ temp1 +', '+ temp2 +', '+ temp3)
							if quad[0] == 'int/':
								self.text.append("\t"+'div '+ temp1 +', '+ temp2 +', '+ temp3)
							if quad[0] == 'int%':
								self.text.append("\t"+'div '+ temp2 +', '+ temp3)
								self.text.append("\t"+'mfhi '+ temp1)
							if quad[0] == 'float+':
								self.text.append("\t"+'add '+ temp1 +', '+ temp2 +', '+ temp3)
							if quad[0] == 'float-':
								self.text.append("\t"+'sub '+ temp1 +', '+ temp2 +', '+ temp3)
							if quad[0] == 'float*':
								self.text.append("\t"+'mul '+ temp1 +', '+ temp2 +', '+ temp3)
							if quad[0] == 'float/':
								self.text.append("\t"+'div '+ temp1 +', '+ temp2 +', '+ temp3)
							if quad[0] == 'float%':
								self.text.append("\t"+'div '+ temp2 +', '+ temp3)
								self.text.append("\t"+'mfhi '+ temp1)
							
						else:
							pass




	def printMipsCode(self):
		print ".data"
		for data in self.data:
			print data
		
		print ".text"
		for text in self.text:
			print text



