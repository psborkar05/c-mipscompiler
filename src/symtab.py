class symTab:
	def __init__(self):
		self.table = {
		'scope' :	'program',
		'type'	:	'function',
		'pscope':	'None',		#parent scope
		'retype':	'undefined'
		}
		self.scopeList = [self.table]
		self.scopeListFull = [self.table]
	def getSize(self,idtype):
		if idtype in ['char']:
			return 1
		elif idtype in ['float','int','bool']:
			return 4
		elif idtype in ['']:
			return 0
	def addIdentifier(self, identifier,idtype,order,blocks,boolPointer,offset):
		currScope = self.scopeList[-1]
		for scpe in self.scopeListFull:
			if scpe['scope']==currScope['scope']:
				currScope2 = scpe
		varSize = 0 
		if idtype in ['char']:
			varSize = 1
		elif idtype in ['float','int']:
			varSize = 4
		elif idtype in [ 'double']:
			varSize = 8
		elif idtype in ['FUNCTION','String']:
			varSize = 4					#address size
		else:
			varSize = 4
		if blocks != 0:
			varSize = varSize * blocks
		if not (currScope.has_key(identifier)):
			currScope[identifier]={
			'size'	:	varSize,
			'type'	:	idtype,
			'order'	:	order,
			'pointer':	boolPointer,
			'offset' : offset
			}
			currScope2[identifier]={
			'size'	:	varSize,
			'type'	:	idtype,
			'order'	:	order,
			'pointer':	boolPointer,
			'offset' : offset
			}
		else :
			print "The variable",identifier,"is declared again"
			exit()


	def addAttrId(self, identifier, attrName, attrVal):
		temp = self.lookup(identifier)
		if temp!= None:
			if temp.has_key(attrName):
				temp[attrName].append(attrVal)
			else:
				temp[attrName] = attrVal
		else:
			print(identifier + "not declared")
	
	
	def lookup(self, identifier):
		return self.findId(identifier, len(self.scopeList) - 1)

	def lookupcurrent(self, identifier):
		tempScope = self.scopeList[scopeLen]
		if tempScope.has_key(identifier):
			return tempScope[identifier]
	
	def findId(self, identifier, scopeLen):
		if scopeLen == -1 :
			return None
		tempScope = self.scopeList[scopeLen]
		if tempScope.has_key(identifier):
			return tempScope[identifier]
		else:
			return self.findId(identifier, scopeLen - 1)

	def addNewScope(self, funcName, scopeType,retype,parameterList):
		tempScope = self.scopeList[-1]
		for scpe in self.scopeListFull:
			if scpe['scope']==tempScope['scope']:
				tempScope2 = scpe
		newTable = {
			'scope'	: funcName,
			'type'	: scopeType,
			'pscope' : tempScope['scope'],
			'retype' : retype,
			'parameterList':parameterList
		}
		tempScope[funcName]={
			'type'	:scopeType,
			'retype' : retype,
			'order'	:	0,
			'pointer':	True
		}
		tempScope2[funcName]={
			'type'	:scopeType,
			'retype' : retype,
			'order'	:	0,
			'pointer':	True
		}
		self.scopeList.append(newTable)
		self.scopeListFull.append(newTable)
	
	def addAttrScope(self, attrName, attrVal):
		tempScope = self.scopeList[-1]
		for scpe in self.scopeListFull:
			if scpe['scope']==tempScope['scope']:
				tempScope2 = scpe
		tempScope[attrName] = attrVal
		tempScope2[attrName] = attrVal

	def delScope(self, funcName):
		del self.scopeList[funcName]
